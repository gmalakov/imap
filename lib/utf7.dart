library utf7;

 import 'base64.dart';

 class Utf7 {

   String setW;
   String setO;
   String setD;
   RegExp regExp;
   Map<String, RegExp> _regexes = new Map();


   static String _encode(String inStr) {
     List<int> inS = inStr.codeUnits;
     int len = inS.length;
     List<int> outS = new List(len * 2);

     int bi = 0;

     for (int i = 0; i < len; i++) {
       int code = inS[i];
       outS[bi++] = code >> 8;
       outS[bi++] = code & 0xFF;
     }

     String encoded = Base64.encode(outS, padChar: "");
     //encoded = encoded.replaceAll("=", "");

     return encoded;
   }

   static String _decode(String inS) {
     List<int> octets = Base64.decode(inS);
     //List<int> octets = BASE64.decode(inS);
     List<int> ret = new List();

     for (int i = 0; i < octets.length-1;)
       ret.add(octets[i++] << 8 | octets[i++]);

     return new String.fromCharCodes(ret);
     //return UTF8.decode(ret, allowMalformed: true);
   }


   static String escape(String chars) => chars.replaceAll(r'[-[\]{}()*+?.,\\^$|#\s]', '\\\$&');

   Utf7() {
     setD = 'A-Za-z0-9' + escape('\'(),-./:?');
     setO = escape('!\"#\$%*;<=>@_\'{|}'); //[]^  these I removed to worke correctly ???
     setW = escape(' \r\n\t');

     //print('[^' + setW + setD + setO + ']+');
     regExp = new RegExp('[^' + setD + setO + setW + ']+', multiLine: true);

   }

   String encode(String inS, [String mask]) {
     if (mask == null) mask = ''; //Init mask if empty
     if (_regexes[mask] == null) _regexes[mask] = new RegExp('[^' + setD + escape(mask) + ']+', multiLine: true);
     if (inS == "+") return "+-"; // + is represented by an empty sequence +-, otherwise call encode().

     String retS = inS.replaceAllMapped( _regexes[mask], (Match match) {
       String chunk = match.group(0);
       if (chunk == "+") return "+";
       return "+"+_encode(chunk)+"-";
     });
     return retS;
   }

   String encodeAll(String inS) {
     if (inS == "+") return ""; // + is represented by an empty sequence +-, otherwise call encode().

     String retS = inS.replaceAllMapped(regExp, (Match match) {
       String chunk = match.group(0);
       if (chunk == "+") return "+";
       return "+"+_encode(chunk)+"-";
     });
     return retS;
   }

   static String decode(String inS) {
     String retS = inS.replaceAllMapped(new RegExp(r'\+([A-Za-z0-9/]*)-?', multiLine: true), (Match match) {
       String chunk = match.group(1);
       if (chunk == "") return "+";
       return _decode(chunk);
     });

     return retS;
   }

   static String imapEncode(String inS) {

     inS = inS
         .replaceAll('&', '&-')
         .replaceAllMapped(new RegExp(r'[^\x20-\x7e]+'), (Match match) {
           String chunk = match.group(0);
           if (chunk == "&") chunk = "";
           else chunk = _encode(chunk).replaceAll("\/", ",");

           return "&"+chunk+"-";
     });

     return inS;
   }

   static String imapDecode(String inS) {
     inS = inS.replaceAllMapped(new RegExp(r'&([^-]*)-', multiLine:true), (Match match) {
       String chunk = match.group(1);
       if (chunk == "") return "&";
       chunk = _decode(chunk.replaceAll(',', "/"));
       return chunk;
     });

     return inS;
   }

  }
