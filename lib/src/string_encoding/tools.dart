part of string_encoding;

enum ISOState {ASCII, escapeStart, escapeMiddle, escapeFinal, lead, trail, Katakana}

bool inRange(int a, int min, int max) =>
    ((a >= min) && (a <= max));
int div(int n, int d) => (n/d).floor();

class BytesInputStream {
 int pos = 0;
 List<int> bytes;

 BytesInputStream(this.bytes);
 BytesInputStream.fromString(String inBytes) {
   bytes = inBytes.codeUnits;
 }

 int get getByte {
   if (pos < 0) throw new Exception("Seeking before beggining of the array");

   if (pos >= bytes.length) return EOF_byte;
    else return bytes[pos];
 }

 String get getChar => new String.fromCharCode(getByte);

 void set offset(int n) {
   pos += n;
  }

  bool match(List<int> test) {
    if (test.length > pos + bytes.length) return false;
    for (int i = 0; i < test.length; i++)
      if (bytes[pos+i] != test[i]) return false;
    return true;
  }

  bool matchString(String test) {
   List<int> tst = test.codeUnits;
   return match(tst);
  }
}

class BytesOutputStream {
  List<int> bytes = new List();
  int pos = 0;

  int emitByte(int byte) {
   if (pos < bytes.length) bytes[pos] = byte;
    else bytes.add(byte);
   pos++;

   return byte;
  }

  int emit(List<int> args) {
    int last = EOF_byte;
    for (int i = 0; i < args.length; i++) {
      last = args[i];
      //We-ll modify this to comply with dart
      if (pos < bytes.length) bytes[pos] = last;
       else bytes.add(last); //Add new element if we are at the end of the array
      pos++;
    }

    return last;
  }

  String emitStr(String sArgs) => new String.fromCharCode(emit(sArgs.codeUnits));
}

class CodePointInputStream extends BytesInputStream {

  CodePointInputStream(List<int> bytes) : super(bytes);
  CodePointInputStream.fromString(String inS) : super.fromString(inS);

  List<int> stringtoCodePoints(String inS) {
    List<int> cps = new List(); //Out codes
    List<int> inList = inS.codeUnits; //Init codes
    int i = 0; int n = inList.length;

    while (i < n) {
      int c = inList[i];
      if (!inRange(c, 0xD800, 0xDFFF))
        cps.add(c);
       else
         if (inRange(c, 0xDC00, 0xDFFF))
           cps.add(0xFFFD);
          else if (i == (n-1)) cps.add(0xFFFD);
            else {
               int d = inList[i+1];
               if (inRange(d, 0xDC00, 0xDFFF)) {
                 var a = c & 0x3FF;
                 var b = d & 0x3FF;
                 i++;
                 cps.add(0x10000 + (a << 10) + b);
               } else
                 cps.add(0xFFFD);

             }
      i++;
    }
    return cps;
  }
}

class CodePointOutputStream {
  List<int> codes = new List();
  Map<String, List> name_to_encoding = new Map();
  Map<String, String> name_to_heading = new Map();
  Map<String, List> label_to_encoding = new Map();
  Map<String, String> label_to_heading = new Map();

  String get str => new String.fromCharCodes(codes);

  void emit(int c) {
    if (c <= 0xFFFF)
      codes.add(c);
    else {
      c -= 0x10000;
      codes.add(0xD800 + ((c >> 10) & 0x3ff));
      codes.add(0xDC00 + (c & 0x3ff));
    }
  }

  Exception encodingError(String msg) => new Exception(msg);

  Exception encoderError(int codePoint) =>
      encodingError("The code point $codePoint could not be encoded.");

  Exception decoderError() => encodingError("DecoderError");

  Map getEncoding(String label) {
    label = label.trim().toLowerCase();
    List enc = label_to_encoding[label];
    Map ret;
    enc.forEach((Map el) {
      if (el is Map) {
        if (el["labels"] is List)
          for (int i = 0; i < (el["labels"] as List).length; i++)
            if (el["labels"][i] == label) ret = el;
      }
    });
    //print(label_to_encoding[label]);
    return ret;
  }

  CodePointOutputStream() {
    ENCODINGS.forEach((category) {
      category.forEach((enc, val) {
        if ((enc == "encodings") &&
            (val is List)) // -> Val is the data for current encodings;
          val.forEach((Map el) {
            el.forEach((String n, dynamic v) {
              if ((n == "name") && (v is String)) {
                   name_to_encoding[v] = val;
                   name_to_heading[v] = category["heading"].toString();
                 }
                if ((n == "labels") && (v is List))
                  v.forEach((String el)  {
                    label_to_encoding[el] = val;
                    label_to_heading[el] = category["heading"].toString();
                  });
            });
          });

        //print("ENC:"+enc.toString());
        //print("VAL:"+val.toString());
      });
    });
  }

  int indexCodePoint(int pointer, List<int> index) => index[pointer];

  int indexPoint(int code_point, List<int> index) {
    int idx = index.indexOf(code_point);
    if (idx == -1)
      return null;
    else
      return idx;
  }

  List index(String name) => ENCODING_INDEXES[name];

  int indexGB18030CodePoint(int pointer) {
    if (((pointer > 39419) && (pointer < 189000)) || (pointer > 1237575))
      return null;

    int offset = 0;
    int codePointOffset = 0;
    List idx = index('gb18030-ranges');

    for (int i = 0; i < idx.length; i++) {
      List<int> entry = idx[i] as List<int>; //Cast here and we'll see;
      if (entry[0] <= pointer) {
        offset = entry[0];
        codePointOffset = entry[1];
      } else
        break;
    }

    return codePointOffset + pointer - offset;
  }

  int indexGB18030CodePointer(int codePoint) {
    int offset = 0;
    int pointerOffset = 0;
    List idx = index('gb18030-ranges');

    for (int i = 0; i < idx.length; i++) {
      List<int> entry;
      if (idx[i] is List<int>) {
        entry = idx[i] as List<int>;
        if (entry[1] <= codePoint) {
          offset = entry[1];
          pointerOffset = entry[0];
        } else
          break;
      } else throw new Exception ("idx[i] is not array."+idx[i].toString());
    }

    return pointerOffset + codePoint - offset;
  }

}

class TextTransformer {
  Map _encoding;
  CodePointOutputStream _cps = new CodePointOutputStream();

  String get encoding => _encoding["name"].toString();
  String optEncoding = DEFAULT_ENCODING;

  TextTransformer (this.optEncoding) {
    _encoding = _cps.getEncoding(optEncoding);
    if ((_encoding == null) || (_encoding["name"] == "replacement"))
      throw new Exception("Uknown encoding $optEncoding");

    //print(_encoding.toString());
  }
}

typedef int DecFunc (BytesInputStream bytePointer);

class TextDecoder extends TextTransformer {
  DecFunc decode;

  TextDecoder(String optEncoding) : super(optEncoding) {
    String heading = _cps.name_to_heading[encoding];
    if (heading == "Legacy single-byte encodings")
      singleByteDecoder(ENCODING_INDEXES[encoding] as List<int>);
    if (encoding == 'utf-8') utf8Decoder();
    if (encoding == 'gb18030') gb18030Decoder();
    if (encoding == 'hz-gb-2312') hzgb2312Decoder();
    if (encoding == 'big5') big5Decoder();
    if (encoding == 'euc-jp') eucpjpDecoder();
    if (encoding == 'iso-2022-jp') iso2022JPDecoder();
    if (encoding == 'shift-jis') shiftJISDecoder();
    if (encoding == 'euc-kr') euCKRDecoder();
    if (encoding == 'utf-16be') utf16Decoder(true);
    if (encoding == 'utf-16le') utf16Decoder(false);
  }

  //bool _streaming = false;
  //bool _BOMSeen = false;
  void singleByteDecoder(List<int> index) {
    decode = ((BytesInputStream bytePointer) {
      int bite = bytePointer.getByte;
      if (bite == EOF_byte) return EOF_code_point;
      bytePointer.offset = 1;
      if (inRange(bite, 0x00, 0x7F)) return bite;
      int codePoint = index[bite - 0x80];
      return codePoint;
    });
  }

  void utf8Decoder() {
   int utf8BytesNeeded = 0;
   int utf8BytesSeen = 0;
   int utf8LowerBoundry = 0;
   int utf8CodePoint = 0;

   decode = ((BytesInputStream bytePointer) {
     int bite = bytePointer.getByte;
     if (bite == EOF_byte) {
       if (utf8BytesNeeded != 0) return null;
       return EOF_code_point;
     }
     bytePointer.offset = 1;

     if (utf8BytesNeeded == 0) {
       if (inRange(bite, 0x00, 0x7F)) return bite;
       if (inRange(bite, 0xC2, 0xDF)) {
         utf8BytesNeeded = 1;
         utf8LowerBoundry = 0x80;
         utf8CodePoint = bite - 0xC0;
       } else if (inRange(bite, 0xE0, 0xEF)) {
         utf8BytesNeeded = 2;
         utf8LowerBoundry = 0x800;
         utf8CodePoint = bite - 0xE0;
       } else if (inRange(bite, 0xF0, 0xF4)) {
         utf8BytesNeeded = 3;
         utf8LowerBoundry = 0x10000;
         utf8CodePoint = bite - 0xF0;
       } else return null;

       utf8CodePoint = utf8CodePoint * (64^utf8BytesNeeded);
       return null;
     }

     if (!inRange(bite, 0x80, 0xBF)) {
       utf8CodePoint = 0;
       utf8BytesNeeded = 0;
       utf8BytesSeen = 0;
       utf8LowerBoundry - 0;
       bytePointer.offset = -1;
       return null;
     }

     utf8BytesSeen++;
     utf8CodePoint = utf8CodePoint + (bite - 0x80) *
         (64^(utf8BytesNeeded-utf8BytesSeen));

     if (utf8BytesSeen != utf8BytesNeeded) return null;

     int codePoint = utf8CodePoint;
     int lowerBoundry = utf8LowerBoundry;
     utf8CodePoint = 0;
     utf8BytesNeeded = 0;
     utf8BytesSeen = 0;
     utf8LowerBoundry = 0;

     if (inRange(codePoint, lowerBoundry, 0x10FFFF) &&
       !inRange(codePoint, 0xD800, 0xDFFF)) return codePoint;

     return null; //Error
   });
  }

  void gb18030Decoder() {
    int gb18030First = 0x00;
    int gb18030Second = 0x00;
    int gb18030Third = 0x00;
    decode = ((BytesInputStream bytePointer) {
      int bite = bytePointer.getByte;
      if ((bite == EOF_byte) && (gb18030First == 0x00) &&
          (gb18030Second == 0x00) && (gb18030Third == 0x00))
         return EOF_code_point;

      if ((bite == EOF_byte) && ((gb18030First != 0x00) || (gb18030Second != 0x00) ||
          (gb18030Third != 0x00))) {
        gb18030First = 0x00;
        gb18030Second = 0x00;
        gb18030Third = 0x00;

        return null;
      }

      bytePointer.offset = 1;
      int codePoint;

      if (gb18030Third != 0x00) {
        codePoint = null;
        if (inRange(bite, 0x30, 0x39))
          codePoint = _cps.indexGB18030CodePoint(
              (((gb18030First - 0x81) * 10 + (gb18030Second - 0x30)) * 126 +
                  (gb18030Third - 0x81)) * 10 + bite - 0x30);

        gb18030First = 0x00;
        gb18030Second = 0x00;
        gb18030Third = 0x00;
        if (codePoint == null)
          // We have error
          bytePointer.offset = -3;

        return codePoint;
      }

      if (gb18030Second != 0x00) {
        if (inRange(bite, 0x81, 0xFE)) {
          gb18030Third = bite;
          return null;
        }

        bytePointer.offset = -2;
        gb18030First = 0x00;
        gb18030Second = 0x00;
        return null;
      }

      if (gb18030First != 0x00) {
        if (inRange(bite, 0x30, 0x39)) {
          gb18030Second = bite;
          return null;
        }

        int lead = gb18030First;
        int pointer = null;
        gb18030First = 0x00;
        int offset = (bite < 0x7F) ? 0x40 : 0x41;
        if (inRange(bite, 0x40, 0x7E) || inRange(bite, 0x80, 0xFE))
          pointer = (lead - 0x81) * 190 + (bite - offset);

        codePoint = (pointer == null) ? null : _cps.indexCodePoint(pointer, ENCODING_INDEXES['gb18030'] as List<int>);
        if (pointer == null) bytePointer.offset = -1;

        return codePoint;
      }

      if (inRange(bite, 0x00, 0x7F)) return bite;
      if (bite == 0x80) return 0x20AC;
      if (inRange(bite, 0x81, 0xFE))
        gb18030First = bite;

      return null;
    });
  }

  void hzgb2312Decoder() {
    int hzgb2312Lead = 0x00;
    bool hzgb2312 = false;

    decode = ((BytesInputStream bytePointer) {
      int bite = bytePointer.getByte;
      if (bite == EOF_byte && hzgb2312Lead == 0x00)
        return EOF_code_point;
      if (bite == EOF_byte && hzgb2312Lead != 0x00) {
        hzgb2312Lead = 0x00;
        return null; //Error
      }

      bytePointer.offset = 1;
      if (hzgb2312Lead == 0x7E) {
        hzgb2312Lead = 0x00;
        if (bite == 0x7B) {
          hzgb2312 = true;
          return null;
        }

        if (bite == 0x7D) {
          hzgb2312 = false;
          return null;
        }

        if (bite == 0x7E) return 0x007E;
        if (bite == 0x0A) return null;

        bytePointer.offset = -1;
        throw new Exception ("Decoding error HZFB2312");
        //return null; //Error
      }

      if (hzgb2312Lead != 0x00) {
        int lead = hzgb2312Lead;
        hzgb2312Lead = 0x00;
        int codePoint = 0x00;
        if (inRange(bite, 0x23, 0x7E))
         codePoint = _cps.indexCodePoint((lead-1) * 1290 + (bite + 0x3F),
             ENCODING_INDEXES['gb18030'] as List<int>);

        if (bite == 0x0A) hzgb2312 = false;
        if (codePoint == null) throw new Exception("Decoding error HZGB12");
        return codePoint;
      }

      if (bite == 0x7E) {
        hzgb2312Lead = 0x7E;
        return null;
      }

      if (hzgb2312) {
        if (inRange(bite, 0x20, 0x7F)) {
          hzgb2312Lead = bite;
          return null;
        }

        if (bite == 0x0A) hzgb2312 = false;

        throw new Exception("Decoding error HZGB2312");
      }

      if (inRange(bite, 0x00, 0x7F)) return bite;
      throw new Exception("Decoding error HZGB23212");
    });
  }

  void big5Decoder() {
    int bigLead = 0x00;
    int bigPending = null;

    decode = ((BytesInputStream bytePointer) {
      // NOTE: Hack to support emitting two code points
      if (bigPending != null) {
        int pending = bigPending;
        bigPending = null;
        return pending;
      }

      int bite = bytePointer.getByte;
      if (bite == EOF_byte && bigLead == 0x00) return EOF_code_point;
      if (bite == EOF_byte && bigLead != 0x00) {
         bigLead = 0x00;
         throw new Exception("Decoder Error Big5");
       }

      bytePointer.offset = 1;

      if (bigLead != 0x00) {
        int lead = bigLead;
        int pointer = null;
        bigLead = 0x00;
        int offset = (bite < 0x7F) ? 0x40 : 0x62;

        if (inRange(bite, 0x40, 0x7E) || inRange(bite, 0xA1, 0xFE))
          pointer = (lead - 0x81) * 157 + (bite - offset);

        if (pointer == 1133) {
          bigPending = 0x304;
          return 0x00CA;
        }

        if (pointer == 1135) {
          bigPending = 0x030C;
          return 0x00CA;
        }

        if (pointer == 1164) {
          bigPending = 0x0304;
          return 0x00EA;
        }

        if (pointer == 1166) {
          bigPending = 0x030C;
          return 0x00EA;
        }

        int codePoint = (pointer == null) ? null :
        _cps.indexCodePoint(pointer, ENCODING_INDEXES['big5'] as List<int>);


        if (pointer == null) bytePointer.offset = -1;
        if (codePoint == null)
          throw new Exception("Decoder Error big5");

        return codePoint;
      }

      if (inRange(bite, 0x00, 0x7F)) return bite;
      if (inRange(bite, 0x81, 0xFE)) {
        bigLead = bite;
        return null;
      }

      throw new Exception("Decoder Error big5");
    });
  }

  void eucpjpDecoder() {
    int eucpjpFirst = 0x00;
    int eucpjpSecond = 0x00;

    decode = ((BytesInputStream bytePointer) {
      int bite = bytePointer.getByte;
      if (bite == EOF_byte) {
        if (eucpjpFirst == 0x00 && eucpjpSecond == 0x00)
          return EOF_code_point;

        eucpjpFirst = 0x00;
        eucpjpSecond = 0x00;
        throw new Exception('EUCPJP Decoder error!');
      }

      bytePointer.offset = 1;
      int lead; int codePoint;
      if (eucpjpSecond != 0x00) {
        lead = eucpjpSecond;
        eucpjpSecond = 0x00;
        codePoint = null;
        if (inRange(lead, 0xA1, 0xFE) && inRange(bite, 0xA1, 0xFE))
          codePoint = _cps.indexCodePoint((lead - 0xA1) * 94 + bite - 0xA1,
            ENCODING_INDEXES['jis0212'] as List<int>);

        if (!inRange(bite, 0xA1, 0xFE)) bytePointer.offset = -1;
        if (codePoint == null) throw new Exception ("EUCPJP Decoder Error!");
        return codePoint;
      }

      if (eucpjpFirst == 0x8E && inRange(bite, 0xA1, 0xDF)) {
        eucpjpFirst = 0x00;
        return 0xFF61 + bite - 0xA1;
      }

      if (eucpjpFirst == 0x8F && inRange(bite, 0xA1, 0xFE)) {
        eucpjpFirst = 0x00;
        eucpjpSecond = bite;
        return null;
      }

      if (eucpjpFirst != 0x00) {
        lead = eucpjpFirst;
        eucpjpFirst = 0x00;
        codePoint = null;
        if (inRange(lead, 0xA1, 0xFE) && inRange(bite, 0xA1, 0xFE))
          codePoint = _cps.indexCodePoint((lead - 0xA1) * 94 + bite - 0xA1,
          ENCODING_INDEXES['jis0208'] as List<int>);
        if (!inRange(bite, 0xA1, 0xFE)) bytePointer.offset = -1;
        if (codePoint == null) throw new Exception('EUCPJP Decoder Error!');
        return codePoint;
      }

      if (inRange(bite, 0x00, 0x7F)) return bite;
      if (bite == 0x8E || bite == 0x8F || inRange(bite, 0xA1, 0xFE)) {
        eucpjpFirst = bite;
        return null;
      }

      throw new Exception("EUCPJP Decoder error!");
    });
  }

  void iso2022JPDecoder() {
    ISOState state = ISOState.ASCII;
    bool jis0212 = false;
    int lead = 0x00;

    decode = ((BytesInputStream bytePointer) {
      int bite = bytePointer.getByte;
      if (bite != EOF_byte) bytePointer.offset = 1;
      switch (state) {
        case ISOState.escapeStart:
          if (bite == 0x24 || bite == 0x28) {
            lead = bite; state = ISOState.escapeMiddle;
            return null;
          }
          if (bite != EOF_byte) bytePointer.offset = -1;
          state = ISOState.ASCII;
          throw new Exception("Decoder Error ISO2022JP");
         break;
        case ISOState.escapeMiddle:
           int cLead = lead;
           lead = 0x00;
           if (cLead == 0x24 && (bite == 0x40 || bite == 0x42)) {
             jis0212 = false;
             state = ISOState.lead;
             return null;
           }
            if (cLead == 0x24 && bite == 0x28) {
             state = ISOState.escapeFinal;
             return null;
           }
           if (cLead == 0x28 && (bite == 0x42 || bite == 0x4A)) {
             state = ISOState.ASCII;
             return null;
           }
           if (cLead == 0x28 && bite == 0x49) {
             state = ISOState.Katakana;
             return null;
           }
           if (bite == EOF_byte) bytePointer.offset = -1;
            else bytePointer.offset = -2;
           state = ISOState.ASCII;
           throw new Exception("Decoder Error ISO2022JP");
          break;
        case ISOState.escapeFinal:
           if (bite == 0x44) {
             jis0212= true;
             state = ISOState.lead;
             return null;
           }
           if (bite == EOF_byte) bytePointer.offset = -2;
            else bytePointer.offset = -3;
           state = ISOState.ASCII;
           throw new Exception("Decoder Error ISO2022JP");
          break;
        case ISOState.lead:
           if (bite == 0x0A) {
             state = ISOState.ASCII;
             throw new Exception("Decoder Error ISO2022JP");
           }
           if (bite == 0x1B) {
             state = ISOState.escapeStart;
             return null;
           }
           if (bite == EOF_byte) return EOF_code_point;
           lead = bite;
           state = ISOState.trail;
           return null;
          break;
        case ISOState.trail:
           state = ISOState.lead;
           if (bite == EOF_byte) throw new Exception("Decoder Error ISO2022JP");
           int codePoint = null;
           int pointer = (lead - 0x21) * 94 + bite - 0x21;
           if (inRange(lead, 0x21, 0x7E) && inRange(bite, 0x21, 0x7E)) {
             codePoint = (jis0212 == false) ?
             _cps.indexCodePoint(
                 pointer, ENCODING_INDEXES['jis0208'] as List<int>) :
             _cps.indexCodePoint(
                 pointer, ENCODING_INDEXES['jis0212'] as List<int>);
           }
           if (codePoint == null) throw new Exception("Decoder Error ISO2022JP");
           return codePoint;
          break;
        case ISOState.Katakana:
           if (bite == 0x1B) {
             state = ISOState.escapeStart;
             return null;
           }
           if (inRange(bite, 0x21, 0x5F)) return 0xFF61 - bite - 0x21;
           if (bite == EOF_byte) return EOF_code_point;
           throw new Exception("Decoder Error ISO2022JP");
          break;
        default:
          if (bite == 0x1B) {
            state = ISOState.escapeStart;
            return null;
           }
          if (inRange(bite, 0x00, 0x7F)) return bite;
          if (bite == EOF_byte) return EOF_code_point;
          throw new Exception ("Decoder Error ISO2022JP");
      }

    });
  }

  void shiftJISDecoder() {
    int lead = 0x00;
    decode = ((BytesInputStream bytePointer) {
      int bite = bytePointer.getByte;
      if (bite == EOF_byte && lead == 0x00) return EOF_code_point;
      if (bite == EOF_byte && lead != 0x00) {
        lead = 0x00;
        throw new Exception("Decoder Error ShiftJSDecoder");
      }
      bytePointer.offset = 1;

      if (lead != 0x00) {
        int cLead = lead;
        lead = 0x00;
        if (inRange(bite, 0x40, 0x7E) || inRange(bite, 0x80, 0xFC)) {
          int offset = (bite < 0x7F) ? 0x40 : 0x41;
          int leadOffset = (cLead < 0xA0) ? 0x81 : 0xC1;
          int codePoint = _cps.indexCodePoint((cLead - leadOffset) * 188 +
              bite - offset, ENCODING_INDEXES['jis0208'] as List<int>);

          if (codePoint == null) throw new Exception(
              "Decoder Error ShiftJSDecoder");
          return codePoint;
        }
        bytePointer.offset = -1;
        throw new Exception("Decoder error ShiftJSDecoder");
      }

      if (inRange(bite, 0x00, 0x80)) return bite;
      if (inRange(bite, 0xA1, 0xDF)) return 0xFF61 + bite - 0xA1;
      if (inRange(bite, 0x81, 0x9F) || inRange(bite, 0xE0, 0xFC)) {
        lead = bite;
        return null;
      }
      throw new Exception("Decoder Error ShiftJSDecoder");
    });
  }

  void euCKRDecoder() {
    int lead = 0x00;
    decode = ((BytesInputStream bytePointer) {
      int bite = bytePointer.getByte;
      if (bite == EOF_byte && lead == 0) return EOF_code_point;
      if (bite == EOF_byte && lead != 0) {
        lead = 0x00;
        throw new Exception("Decoder Error euCKRDecoder");
      }
      bytePointer.offset = 1;

      if (lead != 0x00) {
        int cLead = lead;
        lead = 0x00;
        int pointer = null;

        if (inRange(cLead,0x81, 0xC6)) {
          int temp = 178*(cLead - 0x81);//26 + 26 + 126
          if (inRange(bite,0x41, 0x5A))
            pointer = temp + bite - 0x41;
           else
            if (inRange(bite, 0x61, 0x7A))
              pointer = temp + 26 + bite - 0x61;
             else
               if (inRange(bite, 0x81, 0xFE))
                 pointer = temp + 26 + 26 + bite - 0x81;
          }

        if (inRange(cLead, 0xC7, 0xFD) && inRange(bite, 0xA1, 0xFE))
          pointer = 178 * (0xC7 - 0x81) + (cLead-0xC7) * 94 + (bite - 0xA1);

        int codePoint = (pointer == null) ? null : _cps.indexCodePoint(pointer,
          ENCODING_INDEXES['euc-kr'] as List<int>);

        if (pointer == null) bytePointer.offset = -1;
        if (codePoint == null) throw new Exception("Decoder error EUCKR");

        return codePoint;
      }

      if (inRange(bite, 0x00, 0x7F)) return bite;
      if (inRange(bite, 0x81, 0xFD)) {
        lead = bite;
        return null;
      }

      throw new Exception("Decoder Error EUCKR");
    });
  }

  void utf16Decoder(bool utf16BE) {
    int leadSurrogate;
    int leadByte;
    decode = ((BytesInputStream bytePointer) {
      int bite = bytePointer.getByte;
      if (bite == EOF_byte && leadByte == null && leadSurrogate == null)
        return EOF_code_point;

      if (bite == EOF_byte && (leadByte != null || leadSurrogate != null))
        throw new Exception("Decoder Error UTF16");
      bytePointer.offset = 1;

      if (leadByte == null) {
        leadByte = bite;
        return null;
      }

      int codePoint;
      if (utf16BE)
        codePoint = (leadByte << 8) + bite;
       else
        codePoint = (bite << 8) + leadByte;

       leadByte = null;
       if (leadSurrogate != null) {
         int cLeadSurrogate = leadSurrogate;
         leadSurrogate = null;

         if (inRange(codePoint, 0xDC00, 0xDFFF))
           return 0x10000 + (cLeadSurrogate - 0xD800) * 0x400 + codePoint - 0xDC00;

         bytePointer.offset = -2;
         throw new Exception("Decoder Error UTF16");
       }

       if (inRange(codePoint, 0xD800, 0xDBFF)) {
         leadSurrogate = codePoint;
         return null;
       }

       if (inRange(codePoint, 0xDC00, 0xDFFF))
         throw new Exception("Decoder Error UTF16");

       return codePoint;
    });
  }

  String detectEncoding(String label,BytesInputStream bytePointer) {
    if (bytePointer.match([0xFF, 0xFE])) {
      bytePointer.offset = 2;
      return 'utf16-le';
    }

    if (bytePointer.match([0xFE, 0xFF])) {
      bytePointer.offset = 2;
      return 'utf16-be';
    }

    if (bytePointer.match([0xEF, 0xBB, 0xBF])) {
      bytePointer.offset = 3;
      return 'utf-8';
    }

    return label;
  }
}

typedef int EncFunc(BytesOutputStream outStream, BytesInputStream codePointPointer);

class TextEncoder extends TextTransformer {
  EncFunc encode;

  TextEncoder(String optEncoding) : super(optEncoding) {
    String heading = _cps.name_to_heading[encoding];
    if (heading == "Legacy single-byte encodings")
      singleByteEncoder(ENCODING_INDEXES[encoding] as List<int>);
    if (encoding == 'utf-8') utf8Encoder();
    if (encoding == 'gb18030') gb18030Encoder();
    if (encoding == 'hz-gb-2312') hzgb2312Encoder();
    if (encoding == 'big5') big5Encoder();
    if (encoding == 'euc-jp') eucpjpEncoder();
    if (encoding == 'iso-2022-jp') iso2022JPEncoder();
    if (encoding == 'shift-jis') shiftJISEncoder();
    if (encoding == 'euc-kr') euCKREncoder();
    if (encoding == 'utf-16be') utf16Encoder(true);
    if (encoding == 'utf-16le') utf16Encoder(false);
  }

  void singleByteEncoder(List<int> index) {
    encode = ((BytesOutputStream outStream, BytesInputStream codePointPointer) {
      int codePoint = codePointPointer.getByte;
      if (codePoint == EOF_code_point) return EOF_byte;
      codePointPointer.offset = 1;

      if (inRange(codePoint, 0x0000, 0x007F))
        return outStream.emitByte(codePoint);

      int pointer = _cps.indexPoint(codePoint, index);
      if (pointer == null) return null;

      return outStream.emitByte(pointer + 0x80);
    });
  }

  void utf8Encoder() {
    encode = ((BytesOutputStream outStream, BytesInputStream codePointPointer) {
      int codePoint = codePointPointer.getByte;
      if (codePoint == EOF_code_point) return EOF_byte;
      codePointPointer.offset = 1;

      if (inRange(codePoint, 0xD800, 0xDFFF)) return null;
      if (inRange(codePoint, 0x0000, 0x007F))
        return outStream.emitByte(codePoint);

      int count;
      int offset;

      if (inRange(codePoint, 0x0080, 0x07FF)) {
        count = 1;
        offset = 0xC0;
      } else if (inRange(codePoint, 0x800, 0xFFFF)) {
        count = 2;
        offset = 0xE0;
      } else if (inRange(codePoint, 0x10000, 0x10FFFF)) {
        count = 3;
        offset = 0xF0;
      }

      int result = outStream.emitByte(div(codePoint, 64^count) + offset);
      while (count > 0) {
        int temp = div(codePoint, 64^(count-1));
        result = outStream.emitByte(0x80 + (temp % 64));
        count--;
      }

      return result;
    });
  }

  void gb18030Encoder() {
    encode = ((BytesOutputStream outStream, BytesInputStream codePointPointer) {
      int codePoint = codePointPointer.getByte;
      if (codePoint == EOF_code_point) return EOF_byte;

      int pointer = _cps.indexPoint(codePoint, ENCODING_INDEXES['gb18030'] as List<int>);
      if (pointer != null) {
        int lead = div(pointer, 190) + 0x81;
        int trail = pointer % 190;
        int offset = trail < 0x3F ? 0x40 : 0x41;
        return outStream.emit([lead, trail + offset]);
      }

      pointer = _cps.indexGB18030CodePointer(codePoint);
      int byte1 = div(div(div(pointer, 10), 126), 10);
      pointer = pointer - byte1 * 10 * 126 * 10;
      int byte2 = div(div(pointer, 10), 126);
      pointer = pointer - byte2 * 10 * 126;
      int byte3 = div(pointer, 10);
      int byte4 = pointer - byte3 * 10;

      return outStream.emit([byte1 + 0x81,
          byte2 + 0x30,
          byte3 + 0x81,
          byte4 + 0x30]);
    });
  }

  void hzgb2312Encoder() {
    bool hzgb2312 = false;

    encode = ((BytesOutputStream outStream, BytesInputStream codePointer) {
      int codePoint = codePointer.getByte;
      if (codePoint == EOF_code_point) return EOF_byte;
      codePointer.offset = 1;

      if (inRange(codePoint, 0x0000, 0x007F) && hzgb2312) {
        codePointer.offset = -1;
        hzgb2312 = false;
        return outStream.emit([0x7E, 0x7D]);
      }

      if (codePoint == 0x007E) return outStream.emit([0x7E, 0x7E]);
      if (inRange(codePoint, 0x0000, 0x007F)) return outStream.emitByte(codePoint);

      if(!hzgb2312) {
        codePointer.offset = -1;
        hzgb2312 = true;
        return outStream.emit([0x7E,0x7B]);
      }

      int pointer = _cps.indexPoint(codePoint, ENCODING_INDEXES['gb18030'] as List<int>);
      if (pointer == null) throw new Exception('Encoding error HZGB2312');

      int lead = div(pointer, 190) + 1;
      int trail = pointer % 190 - 0x3F;

      if (!inRange(lead, 0x21, 0x7E) || !inRange(trail, 0x21, 0x7E))
        throw new Exception('Encoding error HZGB2312');

      return outStream.emit([lead, trail]);
    });
  }

  void big5Encoder() {
    encode = ((BytesOutputStream outStream, BytesInputStream codePointer) {
      int codePoint = codePointer.getByte;
      if (codePoint == EOF_code_point) return EOF_byte;

      codePointer.offset = 1;
      if (inRange(codePoint, 0x0000, 0x007F)) return outStream.emitByte(codePoint);
      int pointer = _cps.indexPoint(codePoint, ENCODING_INDEXES['big5'] as List<int>);
      if (pointer == null) throw new Exception("Encoding exception big5");

      int lead = div(pointer, 157) + 0x81;
      int trail = pointer % 157;
      int offset = (trail < 0x3F) ? 0x40 : 0x62;
      return outStream.emit([lead, trail + offset]);
    });
  }

  void eucpjpEncoder() {
    encode = ((BytesOutputStream outStream, BytesInputStream codePointer) {
      int codePoint = codePointer.getByte;
      if (codePoint == EOF_code_point) return EOF_byte;

      codePointer.offset = 1;
      if (inRange(codePoint, 0x0000, 0x007F)) return outStream.emitByte(codePoint);
      if (codePoint == 0x00A5) return outStream.emitByte(0x5C);
      if (codePoint == 0x203E) return outStream.emitByte(0x7E);
      if (inRange(codePoint, 0xFF61, 0xFF9F))
        return outStream.emit([0x8E, codePoint-0xFF61 + 0xA1]);
      int pointer = _cps.indexPoint(codePoint, ENCODING_INDEXES['jis0208'] as List<int>);
      if (pointer == null) throw new Exception('EUCPJP Encoder Error!');

      int lead = div(pointer, 94) + 0xA1;
      int trail = pointer % 94 + 0xA1;
      return outStream.emit([lead, trail]);
    });
  }

  void iso2022JPEncoder() {
    encode = ((BytesOutputStream outStream, BytesInputStream codePointer) {
      ISOState state = ISOState.ASCII;
      int codePoint = codePointer.getByte;
      if (codePoint == EOF_code_point) return EOF_byte;
      codePointer.offset = 1;
      if ((inRange(codePoint, 0x0000, 0x007F) ||
           codePoint == 0x00A5 || codePoint == 0x203E) &&
           state != ISOState.ASCII) {

        codePointer.offset = -1;
        state = ISOState.ASCII;
        return outStream.emit([0x1B, 0x28, 0x42]);
      }

      if (inRange(codePoint, 0x0000, 0x007F)) return outStream.emitByte(codePoint);
      if (codePoint == 0x00A5) return outStream.emitByte(0x5C);
      if (codePoint == 0x203E) return outStream.emitByte(0x7E);

      if (inRange(codePoint, 0xFF61, 0xFF9F) &&
          state != ISOState.Katakana) {

        codePointer.offset = -1;
        state = ISOState.Katakana;
        return outStream.emit([0x1B, 0x28, 0x49]);
      }

      if (inRange(codePoint, 0xFF61, 0xFF9F))
        return outStream.emitByte(codePoint - 0xFF61 - 0x21);

      if (state != ISOState.lead) {
        codePointer.offset = -1;
        state = ISOState.lead;
        return outStream.emit([0x1B, 0x24, 0x42]);
      }

      int pointer = _cps.indexPoint(codePoint, ENCODING_INDEXES['jis0208'] as List<int>);
      if (pointer == null) throw new Exception("Encoding error ISO2022JP");

      int lead = div(pointer, 94) + 0x21;
      int trail = pointer % 94 + 0x21;
      return outStream.emit([lead, trail]);
    });
  }

  void shiftJISEncoder() {
    encode = ((BytesOutputStream outStream, BytesInputStream codePointer) {
      int codePoint = codePointer.getByte;
      if (codePoint == EOF_code_point) return EOF_byte;
      codePointer.offset = 1;

      if (inRange(codePoint, 0x0000, 0x0080))
        return outStream.emitByte(codePoint);
      if (codePoint == 0x00A5) return outStream.emitByte(0x5C);
      if (codePoint == 0x203E) return outStream.emitByte(0x7E);
      if (inRange(codePoint, 0xFF61, 0xFF9F))
        return outStream.emitByte(codePoint - 0xFF61 + 0xA1);

      int pointer = _cps.indexPoint(codePoint, ENCODING_INDEXES["jis0208"] as List<int>);
      if (pointer == null) throw new Exception("Decoder Error shiftJISEncoder");

      int lead = div (pointer, 188);
      int leadOffset = (lead < 0x1F) ? 0x81 : 0xC1;
      int trail = pointer % 188;
      int trailOffset = (trail < 0x3F) ? 0x40 : 0x41;

      return outStream.emit([lead + leadOffset, trail + trailOffset]);
    });
  }

  void euCKREncoder() {
    encode = ((BytesOutputStream outStream, BytesInputStream codePointer) {
      int codePoint = codePointer.getByte;
      if (codePoint == EOF_code_point) return EOF_byte;
      codePointer.offset = 1;

      if (inRange(codePoint, 0x0000, 0x007F)) return outStream.emitByte(codePoint);
      int pointer = _cps.indexPoint(codePoint, ENCODING_INDEXES['euc-kr'] as List<int>);
      if (pointer == null) throw new Exception("Encoder error EUCKR");

      int lead; int trail;
      if (pointer < (178 * (0xC7 - 0x81))) {
        lead = div(pointer, 178) + 0x81;
        trail = pointer % 178;
        int offset = (trail < 26) ? 0x41 : (trail < 52) ? 0x47 : 0x4D;
        return outStream.emit([lead, trail + offset]);
      }

      pointer = pointer - 178 * (0xC7 - 0x81);
      lead = div(pointer, 94) + 0xC7;
      trail = pointer % 94 + 0xA1;
      return outStream.emit([lead, trail]);
    });
  }

  void utf16Encoder(bool utf16be) {
    encode = ((BytesOutputStream outStream, BytesInputStream codePointer) {
      int convertToBytes(int codeUnit) {
        int byte1 = codeUnit >> 8;
        int byte2 = codeUnit & 0xFF;
        if (utf16be) return outStream.emit([byte1, byte2]);
         else return outStream.emit([byte2, byte1]);
      }

      int codePoint = codePointer.getByte;
      if (codePoint == EOF_code_point) return EOF_byte;
      codePointer.offset = 1;

      if (inRange(codePoint, 0xD800, 0xDFFF))
        throw new Exception("Encoder Error UTF16");

      if (codePoint <= 0xFFFF) return convertToBytes(codePoint);

      int lead = div((codePoint - 0x10000), 0x400) + 0xD800;
      int trail = ((codePoint - 0x10000) % 0x400) + 0xDC00;

      convertToBytes(lead);
      return convertToBytes(trail);
    });
  }
}
