library mime_codec;

import 'dart:math';
import 'dart:convert';

import 'string_encoding.dart';
import 'base64.dart';

const List<dynamic> ranges = const <dynamic>[0x09, 0x0A, 0x0D, const [0x20, 0x3C], const [0x3E, 0x7E]];

class MimeCodec {

  static String encodeURIComponent(String inS) {
    return Uri.encodeComponent(inS);
  }

  static String mimeEncode(dynamic data, [String fromCharset]) {
    fromCharset = fromCharset ?? "UTF-8";
    List<int> buffer = CharSet.convert(data ?? "", fromCharset);
    String result = "";
    int ord;

    int len = buffer.length;
    for (int i = 0; i < len; i++) {
      ord = buffer[i];
      if (_checkRanges(ord, ranges) && !((ord == 0x20 || ord == 0x09) &&
          (i == len - 1 || buffer[i + 1] == 0x0A || buffer[i + 1] == 0x0D))) {
        result += new String.fromCharCode(ord);
        continue;
      }

      result +=
          "=" + (ord < 0x10 ? '0' : '') + ord.toRadixString(16).toUpperCase();
    }

    return result;
  }

  static String mimeDecode(String str, [String fromCharset]) {
    fromCharset = fromCharset ?? "UTF-8";
    int encodedBytesCount = 0;
    str.replaceAllMapped(new RegExp(r'=[\da-fA-F]{2}'), (Match m) {
      encodedBytesCount++;
      return "";
    });
    int bufferLength = str.length - encodedBytesCount * 2;
    List<int> buffer = new List(bufferLength);
    int bufferPos = 0;
    String chr;
    String hex;

    for (int i = 0; i < str.length; i++) {
      chr = str[i];
      if (chr == "=") {
        hex = str.substring(i+1, i+3);
        if  (hex.indexOf(new RegExp(r'[\da-fA-F]{2}')) > -1) {
         buffer[bufferPos++] = int.parse(hex, radix: 16);
         i += 2;
         continue;
       }
      }

      buffer[bufferPos++] = chr.codeUnitAt(0);
    }
    return CharSet.decode(buffer, fromCharset);
  }

  static String base64Encode(dynamic data, [String fromChartSet]) {
    List<int> buf;

    if (fromChartSet != "binary" && !(data is String))
      buf = CharSet.convert(data ?? "", fromChartSet);
    else
      if (data is List<int>) buf = data;
        else
          if (data is String) buf = data.codeUnits;

    String b64 = Base64.encode(buf);
    return _addSoftLineBreaks(b64, 'base64');
  }

  static String base64Decode(String str, String fromCharset) {
    List<int> buf = Base64.decode(str ?? "");
    return CharSet.decode(buf, fromCharset);
  }

  static String quotedPrintableEncode(dynamic data, String fromCharset) {
    String mimeEncodedStr = mimeEncode(data, fromCharset);
    mimeEncodedStr = mimeEncodedStr.
    replaceAll(new RegExp(r'\r?\n|\r'), "\r\n"). //fix line breaks -> <CR><LF>
    replaceAllMapped(r'[\t ]+$', (Match m) {
      return m.group(0).replaceAll(' ', '=20').replaceAll('\t', '=09');
      //Escape spaces and tabs
    });

    return _addSoftLineBreaks(mimeEncodedStr, 'qp');
  }

  String quotedPrintableDecode(String str, String fromCharset) {
    str = str ?? "";
    str = str
        .replaceAll(r'[\ ]+$', "")
        .replaceAll(r'\=(?:\r?\n|$)', "");

    return mimeDecode(str, fromCharset);
  }

  static String mimeWordEncode(dynamic data, String mimeWordEncoding, int maxLength,
      String fromCharset) {
    mimeWordEncoding = (mimeWordEncoding ?? "Q").toUpperCase().trim()[0];
    maxLength = maxLength ?? 0;

    String encodedStr;
    String toCharset = 'UTF-8';
    List<String> parts;

    if (maxLength > (7 + toCharset.length))
      maxLength -= 7 + toCharset.length;

    if (mimeWordEncoding == "Q") {
      encodedStr = mimeEncode(data.fromCharset);
      encodedStr =
          encodedStr.replaceAllMapped(r'[^a-z0-9!*+\ -\/=]', (Match m) {
            String chr = m.group(0);
            int code = chr.codeUnitAt(0);
            if (chr == " ")
              return "_";
            else
              return "=" + (code < 0x10 ? '0' : '') +
                  code.toRadixString(16).toUpperCase();
          });
    } else if (mimeWordEncoding == "B") {
      encodedStr = (data is String) ? data : CharSet.decode(data as List<int>, fromCharset);
      maxLength = max(3, ((maxLength - maxLength % 4) / 4 * 3).round());
    }

    if (encodedStr.length > maxLength) {
      if (mimeWordEncoding == "Q")
        encodedStr = _splitMimeEncodedString(encodedStr, maxLength).join(
            '?= =?' + toCharset + '?'
                + mimeWordEncoding + '?');
      else {
        parts = new List();
        for (int i = 0; i < encodedStr.length; i += maxLength)
          parts.add(
              Base64.encode(encodedStr.substring(i, maxLength).codeUnits));

        if (parts.length > 1)
          return '=?' + toCharset + '?' + mimeWordEncoding + '?' +
              parts.join('?= =?' + toCharset + '?' + mimeWordEncoding + '?') +
              '?=';
        else
          encodedStr = parts.join('');
      }
  } else if (mimeWordEncoding == "B")
      encodedStr = Base64.encode(encodedStr.codeUnits);

    String retS = '=?' + toCharset + '?' + mimeWordEncoding +
    '?' + encodedStr + (encodedStr.substring(-2) == '?=' ? '' : '?=');

    return retS;
  }

  static String mimeWordsEncode(dynamic data, String mimeWordEncoding, int maxLength,
      String fromCharset) {
    maxLength = maxLength ?? 0;
    String decodedValue = CharSet.decode(
        CharSet.convert((data ?? ""), fromCharset));
    String encodedValue;

    encodedValue = decodedValue.replaceAllMapped(
        r'([^\s\u0080-\uFFFF]*[\u0080-\uFFFF]+[^\s\u0080-\uFFFF]*(?:\s+[^\s\u0080-\uFFFF]*[\u0080-\uFFFF]+[^\s\u0080-\uFFFF]*\s*)?)+',
            (Match m) {
          if (m
              .group(0)
              .length > 0)
            return mimeWordEncode(
                m.group(0), mimeWordEncoding ?? 'Q', maxLength, fromCharset);
          else
            return '';
        });

    return encodedValue;
  }

  static dynamic mimeWordDecode(String str) {
    str = str ?? "".trim();
    String fromCharset;
    String encoding;
    List<String> match = new List();

    str.replaceAllMapped(
        new RegExp(r'^=\?([\w_\-*]+)\?([QqBb])\?([^?]+)\?=$'), (Match m) {
      match.add(m.group(0));
    });
    if (match.length == 0) return str;
    fromCharset = match[1].split('*')[1];

    encoding = (match[2] ?? 'Q').toUpperCase();
    str = (match[3] ?? "").replaceAll('_', ' ');

    if (encoding == "B")
      return base64Decode(str, fromCharset);
    else if (encoding == "Q")
      return mimeDecode(str, fromCharset);
    else
      return str;
  }

  static String mimeWordsDecode(String str) {
    str = str ?? "".replaceAll(
        new RegExp(r'(=\?[^?]+\?[QqBb]\?[^?]+\?=)\s+(?==\?[^?]+\?[QqBb]\?[^?]*\?=)'), "\$1")
        .replaceAll(new RegExp(r'\?==\?[uU][tT][fF]-8\?[QqBb]\?'), '')
        .replaceAllMapped(new RegExp(r'=\?[\w_\-*]+\?[QqBb]\?[^?]+\?='), (Match m) {
      return mimeWordDecode(m.group(0).replaceAll(new RegExp(r'\s+'), '')).toString();
    });

    return str;
  }

  static String foldLines(String str, [int lineLengthMax, bool afterSpace = false]) {
    str = str ?? "";
    lineLengthMax = lineLengthMax ?? 76;
    int pos = 0;
    int len = str.length;
    String line;
    Match match;
    String result = "";

    while (pos < len) {
      line = str.substring(pos, lineLengthMax);
      if (line.length < lineLengthMax) {
        result += line;
        break;
      }

      match = new RegExp(r'^[^\n\r]*(\r?\n|\r)').firstMatch(line);
      if (match != null) {
        line = match.group(0);
        result += line;
        pos += line.length;
        continue;
      } else {
        Iterable <Match> match = new RegExp(r'(\s+)[^\s]*$').allMatches(line);
        if ((match != null) && (match
            .elementAt(0)
            .group(0)
            .length -
            (afterSpace ? (match.elementAt(1).group(0) ?? "").length : 0) <
            line.length))
          line = line.substring(0, line.length - (match.elementAt(0))
              .group(0)
              .length -
              (afterSpace ? (match.elementAt(1).group(0) ?? "").length : 0));
        else {
          Iterable<Match> match = new RegExp(r'^[^\s]+(\s*)').allMatches(
              line.substring(pos + line.length));
          if (match.length > 0)
            line = line + match.elementAt(0).group(0).substring(0, match
                .elementAt(0)
                .group(0)
                .length -
                (!afterSpace ? (match.elementAt(1).group(0) ?? "").length : 0));
        }

        result += line;
        pos += line.length;
        if (pos < len) line += "\r\n";
      }
    }

    return result;
  }

  static String headerLineEncode(String key, String value, String fromCharset) {
    String encodedValue = mimeWordsEncode(value, 'Q', 52, fromCharset);
    return foldLines(key + ": " + encodedValue, 76);
  }

  static Map<String, String> headerLineDecode(String headerLine) {
    String line = (headerLine ?? "").replaceAll(
        new RegExp(r'(?:\r?\n|\r)[ \t]*'), " ").trim();
    Iterable<Match> match = new RegExp(r'\s*([^:]+):(.*)$').allMatches(line);
    if (match.length >= 3) {
      String key = match.elementAt(1).group(0).trim();
      String value = match.elementAt(1).group(0).trim();
      return {
        "key": key,
        "value": value
      };
    }

    return null;
  }

  static List<int> toTypedArray(String binaryString) {
    return binaryString.codeUnits;
  }

  static String fromTypedArray(List<int> buf) =>
      buf.join();

  static Map<String, dynamic> parseHeaderValue(String str) {
    Map<String, dynamic> response = <String, dynamic>{
      "value": false,
      "params": <String, dynamic>{}
    };

    String key = "";
    String value = "";
    String type = "value";
    String quote = "";
    bool escaped = false;
    String chr;

    for (int i = 0; i < str.length; i++) {
      chr = str[i];
      if (type == 'key') {
        if (chr == "=") {
          key = value.trim().toLowerCase();
          type = 'value';
          value = "";
          continue;
        }
        value += chr;
      } else {
        if (escaped) {
          value += chr;
        } else if (chr == "\\") {
          escaped = true;
          continue;
        } else if (quote.isNotEmpty && chr == quote)
          quote = "";
        else if (quote.isEmpty && chr == '"')
          quote = chr;
        else if (quote.isEmpty && chr == ";") {
          if (key == false)
            response["value"] = value.trim();
          else
            response["params"]["key"] = value.trim();

          type = 'key';
          value = '';
        } else
          value += chr;
        escaped = false;
      }
    }

    if (type == 'value') {
      if (key.isEmpty)
        response["value"] = value.trim();
      else
        response["params"][key] = value.trim();
    } else if (value
        .trim()
        .isNotEmpty)
      response["params"][value.trim().toLowerCase()] = "";

    response["params"].forEach((String par, dynamic val) {
      Match match = new RegExp(r'(\*(\d+)|\*(\d+)\*|\*)$').firstMatch(par);
      String actualKey = par.substring(0, match.start);
      num nr = 0;
      try {
        nr = num.parse(match.group(2));
      } catch (err) {}
      if (nr == 0)
        try {
          nr = num.parse(match.group(3));
        } catch (err) {}

      if ((response["params"][actualKey] == null) ||
          (response["params"][actualKey] != 'object'))
        response["params"][actualKey] = <String, dynamic>{
          "charset": false,
          "values": <dynamic>[]
        };

      value = response["params"][par] as String;
      Match m2 = new RegExp(r"^([^']*)'[^']*'(.*)\$").firstMatch(value);
      if (nr == 0 && match.group(0).substring(-1) == '*' && (m2 != null)) {
        response["params"][actualKey]["charset"] = m2.group(1) ?? "iso-8859-1";
        value = m2.group(2);
      }

      response["params"][actualKey]["values"][nr] = value;
      response["params"].remove(key);
    });

    response["params"].forEach((String par, dynamic val) {
      String value;
      if (val["values"] is List) val["values"] = val["values"].join('');
      if (val["charset"] != null)
        val = ('=?' + val["charset"].toString() + "?Q?" + value)
            .replaceAllMapped(new RegExp(r'[=?_\s]'), (val) {
          String c = val.group(0).codeUnitAt(0).toRadixString(16);
          if (val.group(0) == ' ')
            return "_";
          else
            return "%" + (c.length < 2 ? '0' : '') + c;
        }).replaceAll('%', '=') + '?=';
      else
        response["params"][par] = value;
    });

    return response;
  }

  static dynamic continuationEncode(String key, dynamic data, int maxLength,
      String fromCharset) {
    List<Map> list = new List();
    String encodedStr = (data is String) ? data : CharSet.decode(data as List<int>, fromCharset);
    String chr;
    String line;
    int startPos = 0;
    bool isEncoded = false;

    maxLength = maxLength ?? 50;
    if (new RegExp(r'^[\w.\- ]*$').firstMatch(encodedStr) != null) {
      if (encodedStr.length <= maxLength) {
        Match m = new RegExp(r'[\s";=]').firstMatch(encodedStr);
        return [{
          "key": key,
          "value": (m != null) ? '"' + encodedStr + '"' : encodedStr
        }
        ];
      }

      encodedStr = encodedStr.replaceAllMapped(
          new RegExp(r".{" + maxLength.toString() + "}"), (match) {
        list.add(<String, String>{"line": match.group(0)});
        return "";
      });

      if (encodedStr.isNotEmpty)
        list.add(<String, String>{"line": encodedStr});
    } else {
      line = "utf-8''";
      isEncoded = true;
      startPos = 0;
      for (int i = 0; i < encodedStr.length; i++) {
        chr = encodedStr[i];
        if (isEncoded)
          chr = encodeURIComponent(chr);
        else {
          chr = (chr == " ") ? chr : encodeURIComponent(chr);
          if (chr != encodedStr[i]) {
            if ((encodeURIComponent(line) + chr).length >= maxLength) {
              list.add(<String, dynamic>{"line": line, "encoded": isEncoded});
              line = "";
              startPos = i - 1;
            } else {
              isEncoded = true;
              i = startPos;
              line = '';
              continue;
            }
          }
        }

        if ((line + chr).length >= maxLength) {
          list.add(<String, dynamic>{ "line": line, "encoded": isEncoded});
          line =
          (encodedStr[i] == ' ') ? ' ' : encodeURIComponent(encodedStr[i]);
          chr = line;
          if (chr == encodedStr[i]) {
            isEncoded = false;
            startPos = i - 1;
          } else
            isEncoded = true;
        } else
          line += chr;
      }

      if (line.isNotEmpty)
        list.add(<String, dynamic>{"line": line, "encoded": isEncoded});
    }

    for (int i = 0; i < list.length; i++) {
      list[i]["key"] =
          key + '*' + i.toString() + ((list[i]["encoded"] as bool) ? '*' : '');
      Match m = new RegExp(r'[\s";=]').firstMatch(list[i]["line"] as String);
      list[i]["value"] =
      (m != null) ? '"' + list[i]["line"].toString() + '"' : list[i]["line"];
      list[i].remove("line");
      list[i].remove("encoded");
    }

    return list;
  }

  static List<String> _splitMimeEncodedString(String str, int maxLen) {
    String curLine;
    bool done;
    List<String> lines = new List(0);

    maxLen = max(maxLen ?? 0, 12);
    while (str.isNotEmpty) {
      curLine = str.substring(0, maxLen);
      Match match = new RegExp(r'=[0-9A-F]?$').firstMatch(curLine);
      if (match != null) curLine = curLine.substring(0, match.start);
      done = false;

      while (!done) {
        done = true;

        match = new RegExp(r'^=([0-9A-F]{2})').firstMatch(
            str.substring(curLine.length));
        if (match != null) {
          int chr = int.parse(match.group(1), radix: 16);
          if ((chr < 0xC2) && (chr > 0x7F)) {
            curLine = curLine.substring(0, curLine.length - 3);
            done = false;
          }
        }
      }

      if (curLine.isNotEmpty)
        lines.add(curLine);
      str = str.substring(curLine.length);

    }

    return lines;
  }

  static String _addSoftLineBreaks(String str, String encoding) {
    int lineLengthMax = 76;
    encoding = (encoding ?? "base64").toLowerCase().trim();

    if (encoding == 'qp')
      return _addQPSoftLineBreaks(str,lineLengthMax);
     else
      return _addBase64SoftLineBreaks(str, lineLengthMax);
  }

  static String _addBase64SoftLineBreaks(String base64EncodedStr, int lineLengthMax) {
    base64EncodedStr = (base64EncodedStr ?? "").trim();
    return base64EncodedStr.
      replaceAll(new RegExp(r'.{'+ lineLengthMax.toString() + '}'), '\$&\r\n').trim();
  }

  static String _addQPSoftLineBreaks(String qpEncodedStr, int lineLengthMax) {
    qpEncodedStr = qpEncodedStr ?? "";
    lineLengthMax = lineLengthMax ?? 76;

    int pos = 0;
    int len = qpEncodedStr.length;
    int lineMargin = (lineLengthMax~/3);
    String result = '';
    String line;

    while (pos < len) {
      if (lineLengthMax+pos > len)
        line = qpEncodedStr.substring(pos);
       else
        line = qpEncodedStr.substring(pos, pos+lineLengthMax-1);

      Match match = new RegExp(r'\r\n').firstMatch(line);
      if (match != null) {
        result += line;
        pos += line.length;
        continue;
      }

      if (line.substring(line.length-1) == "\n") {
        result += line;
        pos += line.length;
        continue;
      } else {
        int lm = line.length-lineMargin;
        match = new RegExp(r'\n.*?$').firstMatch(line.substring(lm > 0 ? lm : 0));
        if (match != null) {
          line = line.substring(0, line.length - match.group(0).length + 1);
          result += line;
          pos += line.length;
          continue;
        } else {
          int lm = line.length-lineMargin;
          match = new RegExp(r'[ \t.,!?][^ \t.,!?]*$').firstMatch(line.substring(lm > 0 ? lm : 0));
          if (line.length > lineLengthMax - lineMargin && (match != null))
            line = line.substring(0, line.length - (match.group(0).length -1));
           else if (line.substring(line.length - 1) == '\r')
             line = line.substring(0, line.length -1);
            else {
              match = new RegExp(r'=[\dA-F]{0,2}$').firstMatch(line);
              if (match != null) {
                match = new RegExp(r'=[\dA-F]{0,1}$').firstMatch(line);
                if (match != null) line = line.substring(0, line.length - match.group(0).length);

                while (line.length > 3 && line.length < (len - pos) &&
                    (new RegExp(r'^(?:=[\dA-F]{2}){1,4}$').firstMatch(line) == null)) {

                  match = new RegExp(r'=[\dA-F]{2}$').firstMatch(line);
                  int code = int.parse(match.group(0).substring(1,2), radix: 16);
                  if (code < 128) break;

                  line = line.substring(0, line.length - 3);
                  if (code >= 0xC0) break;
                }
              }
            }
        }
      }

      if ((line.isNotEmpty) && (pos + line.length) < len && line.substring(line.length-1) != '\n') {
        match = new RegExp(r'=[\dA-F]{2}$').firstMatch(line);
        if (line.length == lineLengthMax && match != null)
          line = line.substring(0, line.length - 3);
         else if (line.length == lineLengthMax)
           line = line.substring(0, line.length -1);

         pos += line.length;
         line += '=\r\n';

      } else pos += line.length;

      result += line;
    }

   return result;
  }

  static bool _checkRanges(int nr, List<dynamic> ranges) {
    for (int i = ranges.length -1; i >= 0; i--) {
      List<int> cR = new List();
      if (ranges[i] is int) cR.add(ranges[i] as int);
      if (ranges[i] is List<int>) cR.addAll(ranges[i] as List<int>);

      if (cR.length == 0) continue;
      if ((cR.length == 1) && (nr == cR[0])) return true;
      if ((cR.length == 2) && (nr >= cR[0]) && (nr <= cR[1]))
        return true;
    }

    return false;
  }



}

class CharSet {
  static List<int> encode(String str) => UTF8.encode(str);

  static String decode(List<int> buf, [String fromCharset]) {
    fromCharset = normalizeCharset(fromCharset ?? "UTF-8");
    String retS;
    //if( fromCharset == "UTF-8")
    try { retS = UTF8.decode(buf); } catch(err){}
    if (fromCharset == 'ISO-8859-1')
      try { retS = LATIN1.decode(buf); } catch (err) {}

    if (retS == null)
      try {
        TextDecoder tD = new TextDecoder(fromCharset);
        List<int> oList = new List();
        BytesInputStream bi = new BytesInputStream(buf);
        for (int i = 0; i < buf.length;i++)
          oList.add(tD.decode(bi));
        oList.removeWhere((el) => el == null);
        retS = new String.fromCharCodes(oList);
       } catch(err) { print (err); }

    return retS ?? "";
    }

  static List<int> convert(dynamic data, String fromCharset) {
    fromCharset = normalizeCharset(fromCharset ?? "UTF-8");
    if (data is List<int>) {
      Match match = new RegExp(r'^utf[\-_]?8$').firstMatch(fromCharset);
      if (match != null) return data;

      String bufString = decode(data, fromCharset);
      return encode(bufString);
    }

   return encode(data.toString());
  }

  static String normalizeCharset(String charset) {
     Match match;

     match = new RegExp(r'^utf[\-_]?(\d+)$').firstMatch(charset.toLowerCase());
     if (match != null) return 'UTF-'+match.group(1);
     match = new RegExp(r'^win[\-_]?(\d+)$').firstMatch(charset.toLowerCase());
     if (match != null) return 'WINDOWS-'+match.group(1);
     match = new RegExp(r'^latin[\-_]?(\d+)$').firstMatch(charset.toLowerCase());
     if (match != null) return 'ISO-8859-'+match.group(1);
     return charset; //Shouldn't be here
    }

}