library imap_compiler;

import 'dart:convert';
import 'imap_formal_syntax.dart';

class IRequest {
  String command;
  String tag;
  List<String> attributes = new List();
}

class Node {
  bool isSensitive;
  dynamic value;
  String type;
  List section;
  List partial;

}

class ImapCompiler {
  static List<String> _compile(IRequest response, bool isLogging) {
    List<String> respParts = new List();
    String resp = (response.tag ?? "") + ((response.command != null) ? " "+response.command : '');
    bool needSpace = true;

    void walk(dynamic node) {
      if (resp.isNotEmpty && needSpace) resp += " ";

      //Wal trough list
      if (node is List) {
        needSpace = false;
        resp += "(";
        node.forEach((dynamic el) => walk(el));
        resp += ")";
        return;
      } else needSpace = true;

      //Encode null element
      if ((node == null) && (node is! String) && (node is! num)) {
        resp += "NIL";
        return;
      }

      //Map or string
      if (node is Map) node = JSON.encode(node); //Map to string to encode here
      if (node is String) {
        if (isLogging && node.length > 20) {
          resp += '"(* ' + node.length.toString() + 'B string *)"';
        } else {
          resp += node;
        }
        return;

      }

      //Number
      if (node is num) {
        resp += (node.round() ?? 0).toString();
        return;
      }


      if (node is Node) {
        if (isLogging && node.isSensitive) {
          resp += '"(* value hidden *)"';
          return;
        }

        switch (node.type.toUpperCase()) {
          case 'LITERAL':
            if (isLogging) {
              resp += '"(* ' + node.value.length.toString() + 'B literal *)"';
            } else {
              if (node.value != null) {
                resp += '{0}\r\n';
              } else {
                resp += '{' + node.value.length.toString() + '}\r\n';
              }
              respParts.add(resp);
              resp = node.value.toString() ?? '';
            }
            break;

          case 'STRING':
            if (isLogging && (node.value as String).length > 20) {
              resp += '"(* ' + node.value.length.toString() + 'B string *)"';
            } else
              resp += node.value.toString() ?? '';
            break;

          case 'TEXT':
          case 'SEQUENCE':
            resp += node.value.toString() ?? '';
            break;

          case 'NUMBER':
             resp += (node.value ?? 0).toString();
            break;

          case 'ATOM':
          case 'SECTION':
            String val = node.value.toString() ?? "";
            if (ImapSyntax.verify(((val[0] == '\\') ? val.substring(1) : val) , ImapSyntax.atomChar()) > -1)
              val = JSON.encode(val); //????

            resp += val;

            if (node.section != null) {
              resp += "[";
              if (node.section.length > 0) {
                needSpace = false;
                node.section.forEach((dynamic el) => walk(el));
              }
              resp += "]";
            }

            if (node.partial != null)
              resp += '<' + node.partial.join('.') + '>';

            break;
        }
      }
    }

    response.attributes.forEach((dynamic el) => walk(el));
    if (resp.length > 0)
      respParts.add(resp);

   return respParts;
  }

  static dynamic compile(IRequest response, bool asArray, bool isLogging) {
    if (asArray) return _compile(response, isLogging);
     else return _compile(response, isLogging).join('');
  }
}