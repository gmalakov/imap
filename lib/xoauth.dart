library xoauth;

import 'dart:convert';
import 'dart:math';
import 'package:crypto/crypto.dart';

class XOAuth {
  String hmacSha1(String str, String key) {
    List<int> strBytes = UTF8.encode(str);
    List<int> keyBytes = UTF8.encode(key);

    Hmac hmac = new Hmac(sha1, keyBytes);
    Digest digest = hmac.convert(strBytes);

    return BASE64.encode(digest.bytes);
  }

  String escapeAndJoin(List<dynamic> arr) {
    for (int i = 0; i < arr.length; i++) {
      //Convert other types to string if needed
      arr[i] = Uri.encodeComponent(arr[i].toString());
     }

     return arr.join("&");
  }

 Map<String,String> initOAuthParameters(Map<String, dynamic> options) {
   int rnd = new Random().nextInt(1000000);
   int tStamp = (new DateTime.now().millisecondsSinceEpoch/1000).round();

   return <String, String>{
     "oauth_consumer_key": (options["consumerKey"] ?? "anonymous").toString(),
     "oauth_nonce": (options["nonce"] ?? new DateTime.now().toString() + rnd.toString()).toString(),
     "oauth_signature_method": "HMAC-SHA1",
     "oauth_timestamp": (options["timestamp"] ?? tStamp).toString()
   };
 }

 String generateOAuthBaseStr(String method, String requestUrl, Map<String,dynamic> params) {
    List<String> retPar = new List()
        ..add(method)
        ..add(requestUrl);

    List<String> keys = params.keys.toList();
    keys.sort(); //Sort keys alphabetically

    for (int i = 0; i < keys.length; i++)
      retPar.add(keys[i]+"="+params[keys[i]].toString()); //Build elements key=value

    return retPar.join("&"); //Convert to string joined with &
 }

 String generateXOAutStr(Map<String, dynamic> options) {
    options = options ?? <String,dynamic>{}; // Initialize empty map if null

    Map<String,String> params = initOAuthParameters(options);
    String requestUrl  = (options["requestUrl"] ?? "https://mail.google.com/mail/b/" + (options["user"] ?? "").toString() +
      "/imap/").toString();

    String baseStr, signatureKey, paramsStr;
    if ((options["token"] != null) && (options["reuqestorId"] == null)) params["oauth_token"] = options["token"].toString();

    baseStr = generateOAuthBaseStr((options["method"] ?? "GET").toString(), requestUrl, params);

    if (options["requestorId"] != null)
      baseStr += Uri.encodeComponent("&xoauth_requestor_id=" + Uri.encodeComponent(options["requestorId"].toString()));

    signatureKey = escapeAndJoin(<dynamic>[options["consumerSecret"] ?? "anonymous", options["tokenSecret"] ?? ""]);
    params["oauth_signature"] = hmacSha1(baseStr, signatureKey);

    List<String> parKeys = params.keys.toList();
    parKeys.sort(); //Sort keys
    List<String> oPars = new List();
    for (int i = 0; i < parKeys.length; i++)
      oPars.add(parKeys[i]+"=\""+params[parKeys[i]]+"\""); //Collect parameters with keys
    paramsStr = oPars.join(","); //Convert to string

   String reqStr = "";
   if (options["requestor_id"] != null) reqStr = "?xoauth_requestor_id=" +
    Uri.encodeComponent(options["requestorId"].toString());

   List<String> retList = new List()
     ..add((options["method"] ?? "GET").toString())
     ..add(requestUrl+reqStr)
     ..add(paramsStr);

   return BASE64.encode(UTF8.encode(retList.join(" ")));
 }

}