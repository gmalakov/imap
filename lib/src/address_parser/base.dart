part of address_parser;

class Node {
  String type;
  String value;

  Node();
  Node.fill(this.type, this.value);

  Map<String,String> toMap() => {
    "type": type,
    "value": value
  };

  String toString() => toMap().toString();
}

class Address {
  String address;
  String name;
  List<Address> group;

  Address();
  Address.fill(this.address, this.name, this.group);

  Map<String, String> toMap() => {
    "address": address,
    "name": name,
    "group": group.toString()
  };

  String toString() => toMap().toString();
}
