library mime_types;

part 'src/mime_types/defs.dart';

class Mime {
  static String detectExtension(String mimeType) {
    mimeType = mimeType ?? "".toLowerCase().replaceAll(" ", "");
    dynamic ext = MimeTypesList[mimeType];
    if (ext == null) return "bin";
    if (ext is String) return ext;

    if (ext is List<String>) {
      List<String> mimeParts = mimeType.split('/');
      for (int i = 0; i < ext.length; i ++)
        if (mimeParts[1] == ext[i]) return ext[i];

      return ext[0]; // if Still is not found
    }

    return "bin";
  }

  static const String defType = "application/octet-stream";

  static String detecMimeType(String extension) {
    extension =
        (extension ?? "").toLowerCase().replaceAll(" ", "").replaceAll(".", "");

    dynamic mimeType = MimeTypesExtensions[extension];
    if (mimeType == null) return defType; //Not found
    if (mimeType is String) return mimeType; //Is ready for return

    if (mimeType is List<String>) {
      for (int i = 0; i < mimeType.length; i ++) {
        List<String> mimeParts = mimeType[i].split("/");
        if (mimeParts[1] == extension) return mimeType[i]; //Found
      }
      return mimeType[0];
    }

    return defType;
  }

}