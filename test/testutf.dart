import '../lib/utf7.dart';
import "package:test/test.dart";


const List<String> decoded = const [
 "A\u2262\u0391.",
 "\u65E5\u672C\u8A9E",
 "Hi Mom -\u263A-!",
 "Item 3 is \u00A31.",
 "Jyv\u00E4skyl\u00E4",
 "'\u4F60\u597D' heißt \"Hallo\"",
 "Hot & Spicy & Fruity",
 "\uffff\uedca\u9876\u5432\u1fed",
 "\u00E4&\u00E4&\u00E4",

];

const List<String> encoded = const [
  "A+ImIDkQ-.",
  "+ZeVnLIqe-",
  "Hi+ACA-Mom+ACA--+Jjo--+ACE-",
  "Item+ACA-3+ACA-is+ACAAow-1.",
  "Jyv+AOQ-skyl+AOQ-",
  "'+T2BZfQ-'+ACA-hei+AN8-t+ACAAIg-Hallo+ACI-",
  "Hot+ACAAJgAg-Spicy+ACAAJgAg-Fruity",
  "+///typh2VDIf7Q-",
  "+AOQAJgDkACYA5A-"
];

const List<String> encodedAll = const [
 "A+ImIDkQ-.",
  "+ZeVnLIqe-",
  "Hi Mom -+Jjo--!",
  "Item 3 is +AKM-1.",
  "Jyv+AOQ-skyl+AOQ-",
  "'+T2BZfQ-' hei+AN8-t \"Hallo\"",
  "Hot +ACY- Spicy +ACY- Fruity",
  "+///typh2VDIf7Q-",
  "+AOQAJgDkACYA5A-"
];

const List<String> imapEncoded = const [
  "A&ImIDkQ-.",
  "&ZeVnLIqe-",
  "Hi Mom -&Jjo--!",
  "Item 3 is &AKM-1.",
  "Jyv&AOQ-skyl&AOQ-",
  "'&T2BZfQ-' hei&AN8-t \"Hallo\"",
  "Hot &- Spicy &- Fruity",
  "&,,,typh2VDIf7Q-",
  "&AOQ-&-&AOQ-&-&AOQ-"
];

void main() {

  for (int ca = 0; ca < decoded.length; ca++) {
    test("Test Imap Encode with ca=$ca InString=(${decoded[ca]})", () {

      expect(Utf7.imapEncode(decoded[ca]), equals(imapEncoded[ca]));
    });
  }

  for (int ca = 0; ca < imapEncoded.length; ca++) {
    test("Test Imap Decode with ca=$ca InString=(${imapEncoded[ca]})", () {

      expect(Utf7.imapDecode(imapEncoded[ca]), equals(decoded[ca]));
    });
  }

  //Neeed non static access for regexes
  var u = new Utf7();

  for (int ca = 0; ca < decoded.length; ca++) {
    test("Test Encode with ca=$ca InString=(${decoded[ca]})", () {

      expect(u.encode(decoded[ca]), equals(encoded[ca]));
    });
  }

  for (int ca = 0; ca < decoded.length; ca++) {
    test("Test EncodeAll with ca=$ca InString=(${decoded[ca]})", () {

      expect(u.encodeAll(decoded[ca]), equals(encodedAll[ca]));
    });
  }


}