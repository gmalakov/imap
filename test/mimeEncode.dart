import '../lib/mime_codec.dart';
import 'dart:convert';
import 'package:test/test.dart';

const List<dynamic> raw = const <dynamic>["tere ÕÄÖÕ","tere  ", const <int>[0xBD, 0xC5], "신"];
const List<String> enc = const ["utf-8", "utf-8", "ks_c_5601-1987", "ks_c_5601-1987"];
const List<String> encoded = const ["tere =C3=95=C3=84=C3=96=C3=95", "tere =20", '=EC=8B=A0', '=EC=8B=A0'];

const List<dynamic> qList = const<dynamic> ['tere ÕÄ \t\nÕÄ \t\nÖÕ', 'õäöüõäöüõäöüõäöüõäöüõäöüõäöõ',
      const [0x54, 0x65, 0x72, 0x65, 0x20, 0xD5, 0xC4, 0xD6, 0xDC, 0x21],
      'Tere ÕÄÖÜŠŽ!',const [0x54, 0x65, 0x72, 0x65, 0x20, 0xD0, 0xDE, 0x21],
      'a==========================', 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'+
      'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'+
      'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789',
      'Title: <a href="http://www.elezea.com/2012/09/iphone-5-local-maximum/">The future of e-commerce is storytelling</a> <br>'];

const List<String> qPrint = const ['tere =C3=95=C3=84 =09\r\n=C3=95=C3=84 =09\r\n=C3=96=C3=95', '=C3=B5=C3=A4=C3=B6=C3=BC=C3=B5=C3=A4=C3=B6=C3=BC=C3=B5=C3=A4=C3=B6=C3=BC=C3=\r\n' +
    '=B5=C3=A4=C3=B6=C3=BC=C3=B5=C3=A4=C3=B6=C3=BC=C3=B5=C3=A4=C3=B6=C3=BC=C3=B5=\r\n' +
    '=C3=A4=C3=B6=C3=B5', 'Tere =C3=95=C3=84=C3=96=C3=9C!', 'Tere =C3=95=C3=84=C3=96=C3=9C=C5=A0=C5=BD!',
    'Tere =C5=A0=C5=BD!','a=3D=3D=3D=3D=3D=3D=3D=3D=3D=3D=3D=3D=3D=3D=3D=3D=3D=3D=3D=3D=3D=3D=3D=3D=\r\n=3D=3D',
    'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLM=\r\n'+
    'NOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ=\r\n'+
    'abcdefghijklmnopqrstuvwxyz0123456789',
    'Title: <a href=3D"http://www.elezea.com/2012/09/iphone-5-local-maximum/">Th=\r\ne future of e-commerce is storytelling</a> <br>'];

const List<String> qEnc = const ['utf-8', 'utf-8', 'Latin1', 'utf-8', 'Win-1257', 'utf-8','utf-8','utf-8'];

void main() {
  //Mime Encode
  for (int i = 0; i < raw.length; i++) {
    test("Test Mime Encode sequence $i", () {
      String tmp = MimeCodec.mimeEncode(raw[i],enc[i]);
      expect(tmp, equals(encoded[i]));
    });

  }

  //Mime Decode
  for (int i = 0; i < raw.length; i++) {
    test("Test Mime Decode sequence $i", () {
      String tmp = MimeCodec.mimeDecode(encoded[i],enc[i]);
      if (raw[i] is List<int>) expect(tmp, equals(CharSet.decode(raw[i] as List<int>, enc[i])));
       else expect(tmp, equals(raw[i]));
    });
  }

  //Quoted printable
  for (int i = 0; i < qList.length; i++) {
    test("Test Quoted Printable Encode sequence $i", () {
      String tmp = MimeCodec.quotedPrintableEncode(qList[i], qEnc[i]);
      expect(tmp, equals(qPrint[i]));
    });
  }

}
