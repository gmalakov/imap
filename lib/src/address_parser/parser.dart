part of address_parser;

class AddressParser {

  List<Address> parseAddress(String str) {
    List<Node> tokens = new Tokenizer().tokenize(str);

    List<List<Node>> addresses = new List();
    List<Address> parsedAddresses = new List();
    List<Node> address = new List();

    tokens.forEach((Node token) {
      if ((token.type == "operator") && ((token.value == ",") || (token.value == ";"))) {
        if (address.isNotEmpty) addresses.add(address);
        address = new List();
      } else address.add(token);
    });

    if (address.isNotEmpty) addresses.add(address);

    addresses.forEach((address) {
      List<Address> addr = _handleAddress(address);
      if (addr.isNotEmpty) parsedAddresses.addAll(addr);
    });

    return parsedAddresses;
  }


  List<Address> _handleAddress(List<Node> tokens) {
    bool isGroup = false;
    String state = "text";
    Map<String, List<String>> data = new Map();
    List<Address> addresses = new List();

    //Init parameter lists
    data["address"] = new List<String>();
    data["comment"] = new List<String>();
    data["group"] = new List<String>();
    data["text"] = new List<String>();

    for (int i = 0; i < tokens.length; i++) {
      Node token = tokens[i];

      if (token.type == "operator")
        switch (token.value) {
          case "<":
            state = "address";
            break;
          case "(":
            state = "comment";
            break;
          case ":":
            state = "group";
            isGroup = true;
            break;
          default:
            state = "text";
        }
      else
      if ((token.value != null) && (token.value.isNotEmpty))
        data[state].add(token.value);
    }

    // If there is no text but a comment, replace the two
    if ((data["text"].isEmpty) && (data["comment"].isNotEmpty)) {
      data["text"] = data["comment"];
      data["comment"] = new List();
    }

    if (isGroup) {
      String text = data["text"].join(" ");
      //if (text.isEmpty) text = address.name;
      String group = "";
      if (data["group"].isNotEmpty) group = data["group"].join(",").trim();
      addresses.add(new Address.fill("", text, parseAddress(group)));
    } else {
      // If no address was found, try to detect one from regular text
      if ((data["address"].length == 0) && (data["text"].length > 0)) {
        for (int i = data["text"].length - 1; i >= 0; i--) {
          int idx = data["text"][i].indexOf(new RegExp(r'^[^@\s]+@[^@\s]+$'));
          if (idx == -1) idx = data["text"][i].indexOf(new RegExp(r'\s*\b[^@\s]+@[^@\s]+\b\s*'));
          if (idx > -1) {
            data["address"].add(data["text"][i].substring(idx).trim());
            data["text"][i] = data["text"][i].substring(0, idx).trim();
          }
        }
      }
    }

    //REMOVE EMPTY
    data["address"].removeWhere((el) => el.isEmpty);
    data["text"].removeWhere((el) => el.isEmpty);

    // Keep only the first address occurence, push others to regular text
    if (data["address"].length > 1) {
      String addr = data["address"][0];
      data["address"].removeAt(0);

      data["text"].add(data["address"].join(" ")); // Put other addresses to text
      data["address"].insert(0, addr); //Put back handled addreess
    }

    if (data["address"].isEmpty && isGroup) return addresses;
    else
      {
        String addr = data["address"].join(",");
        if (addr.isEmpty) addr = data["text"].join(",");
        String name = data["text"].join(",");
        if (name.isEmpty) name = data["address"].join(",");

        //If address and name are the same and address contains @ zero name
        //Otherwise zero address -> string is not a email address
        if (addr == name) {
          if (addr.indexOf("@") > -1) name = "";
           else addr = "";
        }

        if ((data["address"].length == 0) && ((name.isNotEmpty) || (addr.isNotEmpty)))
         addresses.add(new Address.fill(addr, name, null));
        //Insert into addresses
        for (int i = 0; i < data["address"].length; i++)
          addresses.add(new Address.fill(data["address"][i], name, null));
      }

    return addresses;
  }
}
