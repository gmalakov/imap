import '../lib/mime_types.dart';
import "package:test/test.dart";

const Ext = const ["doc", "jpeg", "js"];
const Types = const ["application/msword", "image/jpeg", "application/javascript"];

void main () {
  for (int ca = 0; ca < Ext.length; ca++) {
    //print (Ext[ca]+"-->"+Mime.detecMimeType(Ext[ca]));
    //print (Types[ca]+"-->"+Mime.detectExtension(Types[ca]));
    test("Test Extension to MimeType ca=$ca", () {
      expect(Types[ca], equals(Mime.detecMimeType(Ext[ca])));
    });

    test("Test MimeType to Extension ca=$ca", () {
      expect(Ext[ca], equals(Mime.detectExtension(Types[ca])));
    });

  }
}
