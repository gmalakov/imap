library imap_client;

import 'dart:async';
import 'dart:math';
import 'tcp_socket.dart';
import 'imap_compiler.dart' as compiler;
import 'imap_parser.dart' as parser;
import 'mime_codec.dart' as mime;

const SPECIAL_USE_FLAGS = const ['\\All', '\\Archive', '\\Drafts', '\\Flagged', '\\Junk', '\\Sent', '\\Trash'];
const Map<String, List<String>> SPECIAL_USE_BOXES = const <String, List<String>> {
  '\\Sent': const ['aika', 'bidaliak', 'bidalita', 'dihantar', 'e rometsweng', 'e tindami', 'elküldött', 'elküldöttek', 'enviadas', 'enviadas', 'enviados', 'enviats', 'envoyés', 'ethunyelweyo', 'expediate', 'ezipuru', 'gesendete', 'gestuur', 'gönderilmiş öğeler', 'göndərilənlər', 'iberilen', 'inviati', 'išsiųstieji', 'kuthunyelwe', 'lasa', 'lähetetyt', 'messages envoyés', 'naipadala', 'nalefa', 'napadala', 'nosūtītās ziņas', 'odeslané', 'padala', 'poslane', 'poslano', 'poslano', 'poslané', 'poslato', 'saadetud', 'saadetud kirjad', 'sendt', 'sendt', 'sent', 'sent items', 'sent messages', 'sända poster', 'sänt', 'terkirim', 'ti fi ranṣẹ', 'të dërguara', 'verzonden', 'vilivyotumwa', 'wysłane', 'đã gửi', 'σταλθέντα', 'жиберилген', 'жіберілгендер', 'изпратени', 'илгээсэн', 'ирсол шуд', 'испратено', 'надіслані', 'отправленные', 'пасланыя', 'юборилган', 'ուղարկված', 'נשלחו', 'פריטים שנשלחו', 'المرسلة', 'بھیجے گئے', 'سوزمژہ', 'لېګل شوی', 'موارد ارسال شده', 'पाठविले', 'पाठविलेले', 'प्रेषित', 'भेजा गया', 'প্রেরিত', 'প্রেরিত', 'প্ৰেৰিত', 'ਭੇਜੇ', 'મોકલેલા', 'ପଠାଗଲା', 'அனுப்பியவை', 'పంపించబడింది', 'ಕಳುಹಿಸಲಾದ', 'അയച്ചു', 'යැවු පණිවුඩ', 'ส่งแล้ว', 'გაგზავნილი', 'የተላኩ', 'បាន​ផ្ញើ', '寄件備份', '寄件備份', '已发信息', '送信済みﾒｰﾙ', '발신 메시지', '보낸 편지함'],
  '\\Trash': const ['articole șterse', 'bin', 'borttagna objekt', 'deleted', 'deleted items', 'deleted messages', 'elementi eliminati', 'elementos borrados', 'elementos eliminados', 'gelöschte objekte', 'item dipadam', 'itens apagados', 'itens excluídos', 'mục đã xóa', 'odstraněné položky', 'pesan terhapus', 'poistetut', 'praht', 'prügikast', 'silinmiş öğeler', 'slettede beskeder', 'slettede elementer', 'trash', 'törölt elemek', 'usunięte wiadomości', 'verwijderde items', 'vymazané správy', 'éléments supprimés', 'видалені', 'жойылғандар', 'удаленные', 'פריטים שנמחקו', 'العناصر المحذوفة', 'موارد حذف شده', 'รายการที่ลบ', '已删除邮件', '已刪除項目', '已刪除項目'],
  '\\Junk': const ['bulk mail', 'correo no deseado', 'courrier indésirable', 'istenmeyen', 'istenmeyen e-posta', 'junk', 'levélszemét', 'nevyžiadaná pošta', 'nevyžádaná pošta', 'no deseado', 'posta indesiderata', 'pourriel', 'roskaposti', 'skräppost', 'spam', 'spam', 'spamowanie', 'søppelpost', 'thư rác', 'спам', 'דואר זבל', 'الرسائل العشوائية', 'هرزنامه', 'สแปม', '‎垃圾郵件', '垃圾邮件', '垃圾電郵'],
  '\\Drafts': const ['ba brouillon', 'borrador', 'borrador', 'borradores', 'bozze', 'brouillons', 'bản thảo', 'ciorne', 'concepten', 'draf', 'drafts', 'drög', 'entwürfe', 'esborranys', 'garalamalar', 'ihe edeturu', 'iidrafti', 'izinhlaka', 'juodraščiai', 'kladd', 'kladder', 'koncepty', 'koncepty', 'konsep', 'konsepte', 'kopie robocze', 'layihələr', 'luonnokset', 'melnraksti', 'meralo', 'mesazhe të padërguara', 'mga draft', 'mustandid', 'nacrti', 'nacrti', 'osnutki', 'piszkozatok', 'rascunhos', 'rasimu', 'skice', 'taslaklar', 'tsararrun saƙonni', 'utkast', 'vakiraoka', 'vázlatok', 'zirriborroak', 'àwọn àkọpamọ́', 'πρόχειρα', 'жобалар', 'нацрти', 'нооргууд', 'сиёҳнавис', 'хомаки хатлар', 'чарнавікі', 'чернетки', 'чернови', 'черновики', 'черновиктер', 'սևագրեր', 'טיוטות', 'مسودات', 'مسودات', 'موسودې', 'پیش نویسها', 'ڈرافٹ/', 'ड्राफ़्ट', 'प्रारूप', 'খসড়া', 'খসড়া', 'ড্ৰাফ্ট', 'ਡ੍ਰਾਫਟ', 'ડ્રાફ્ટસ', 'ଡ୍ରାଫ୍ଟ', 'வரைவுகள்', 'చిత్తు ప్రతులు', 'ಕರಡುಗಳು', 'കരടുകള്‍', 'කෙටුම් පත්', 'ฉบับร่าง', 'მონახაზები', 'ረቂቆች', 'សារព្រាង', '下書き', '草稿', '草稿', '草稿', '임시 보관함']
};

const MESSAGE_START = 'start';
const MESSAGE_INFLATE = 'inflate';
const MESSAGE_INFLATED_DATA_READY = 'inflated_ready';
const MESSAGE_DEFLATE = 'deflate';
const MESSAGE_DEFLATED_DATA_READY = 'deflated_ready';

const EOL = '\r\n';
const LINE_FEED = 10;
const CARRIAGE_RETURN = 13;
const LEFT_CURLY_BRACKET = 123;
const RIGHT_CURLY_BRACKET = 125;

const ASCII_PLUS = 43;

enum BufferState {LITERAL, LITERAL_LEN_1, LITERAL_LEN_2, DEFAULT }

enum ConnState {NONE, CONNECTING, NOT_AUTHENTICATED, AUTHENTICATED, SELECTED, LOGOUT}
const Duration ConnTimeout = const Duration(seconds:  90);
const Duration NoopTimeout = const Duration(seconds: 60);
const Duration IdleTimeout = const Duration(seconds: 60);
const Duration EnterIdleTimeout = const Duration(seconds: 1);
const Duration SocketLowerBouundTimeout = const Duration(seconds: 10);
const SocketMultiplier = 0.1;


const Events = const ['capability', 'ok', 'exists', 'expunge', 'fetch'];

/*
response.
 ..payload.
 	  ..search <list>
   	..fetch <list>
   	..ID <list>
   	..list <list>
   	..lsub <list>
   	..namespace <list>

 ..code
 ..capability
 ..humanReadable
 ..attributes
 ..nr


 request
   ..command
   ..attributes
 */


typedef ICallBack(parser.IResponse response);
typedef Future PreCheck();
typedef OnAction();

class IRecord {
  String tag;
  compiler.IRequest request;
  Map<String, List<parser.IResponse>> payload;
  Map<String, dynamic> options;
  PreCheck preCheck;
  String data;
  ICallBack callback;
}

typedef Handler(dynamic msg);

class ImapClient {
  static int sessionCounter = 0;
  static final onDataTimeout = SocketLowerBouundTimeout+new Duration(milliseconds:(4096*SocketMultiplier).floor());

  List<IRecord> _commands = new List();

  Map<String, String> serverId = new Map();
  Map<String, String> options;
  int sessionId;
  ConnState _state = ConnState.NONE;
  bool _connectionReady;

  bool _authenticated = false;
  List<String> _capability;

  bool _selectedMailbox = false;
  bool _idle = false;
  int _tagCounter = 0;
  Map<String, Function> _globalAcceptUntagged = new Map(); // ??
  bool get _canSend => ((_state == ConnState.AUTHENTICATED) || (_state == ConnState.SELECTED));
  IRecord _currentCmd;
  bool _compressed = false;

  List<List<int>> _incommingBuffers;
  List<int> _lengthBuffer;
  BufferState _bufState = BufferState.DEFAULT;
  int _literalRemaining = 0;
  //bool _connReady = false;

  void _enterIdle() {
    _idle = true;
    //Clear old timeout
    if (_idleTimeout != null) _idleTimeout.cancel();
    _idleTimeout = null;

    //Create new timeout if needed
    if (onIdle != null)
      _idleTimeout = new Timer(IdleTimeout, () => onIdle());
  }

  void _clearIdle() {
    _idle = false;
    //Clear old timeout
    if (_idleTimeout != null) _idleTimeout.cancel();
    _idleTimeout = null;
  }

  Timer _idleTimeout;
  Timer _connectionTimeout;
  Timer _socketTimeout;

  MailSocket _socket;
  String _host;
  int _port;
  bool get _secure => ((_port ?? 0) == 993);

  Handler onError;
  OnAction onIdle;
  OnAction onReady;

  ImapClient(this._host, this._port, this.options) {
    _host = _host ?? "localhost";
    _port = _port ?? 993;
    sessionId = ++sessionCounter;

  }

  void _onError(dynamic err) {
    if (_idleTimeout != null) _idleTimeout.cancel();
    _idleTimeout = null; //Clear timeout
    if (onError != null) onError(err);
  }

  Future<bool> connect() async {
    _connectionTimeout = new Timer(ConnTimeout, () {
      _connectionTimeout.cancel(); _connectionTimeout = null;
      throw new Exception('Timeout connecting to server!');
    });

    _state = ConnState.CONNECTING;
    _socket = new MailSocket(_host, _port, _secure);

    bool res = await _socket.connect();
    _connectionTimeout.cancel(); _connectionTimeout = null; //Clear connection timeout on connect
    if (res) return true;
     else {
       _state = ConnState.NOT_AUTHENTICATED;
       return false;
    }

  }

  void close() {
    _commands.forEach((el) => el.callback(new parser.IResponse()..code = "closing"));

    //Disable all timers
    if (_idleTimeout != null) _idleTimeout.cancel();
    if (_connectionTimeout != null) _connectionTimeout.cancel();
    _connectionTimeout = null;
    _idleTimeout = null;

    _compressed = false;
    _socket.close();
  }

  void logOut() {
    _state = ConnState.LOGOUT;
    exec(new compiler.IRequest()..command="LOGOUT", [], options);
    //close();
  }

  void addRequest(compiler.IRequest request, List<String> acceptUntagged, Map<String,dynamic> options, ICallBack callBack) {
    IRecord rec = new IRecord()
        ..tag = 'W'+(++_tagCounter).toString()
        ..request = request
        ..options = options;

    acceptUntagged.forEach((command) => rec.payload[command] = new List());
    int index = _commands.indexOf(options["ctx"] as IRecord);
    if (index > -1) rec
      ..tag += '.p'
      ..request.tag += '.p';

    rec.callback = ((parser.IResponse resp) {
      if (['NO', 'BAD'].indexOf(resp.code.toUpperCase().trim()) > -1) throw new Exception(resp.code);
      //If not null response then run it
      if (callBack != null) callBack(resp);
    });

  _commands.add(rec);
  if (_canSend) _sendRequest();
}

 Future _sendRequest() async {
    if (_commands.length == 0) {
        _enterIdle();
        return;
      }

      _clearIdle();
    IRecord rec = _commands[0]; //read first command
    _commands.removeAt(0); //Remove got command

    //Run precheck command
    if (rec.preCheck != null)
      try { await rec.preCheck(); } catch(err) {
      rec.callback(new parser.IResponse()..code = "Precheck error "+err.toString());
      if (_commands.length > 0) _sendRequest; //Call myself if queue is not empty
      return;
    }

    List<String> data;
    try {
      data = compiler.ImapCompiler.compile(rec.request, true, false) as List<String>;
    } catch (err) {
      print(err.toString()+" Compiling imap command!");
    }

    send(data[0] + ((data.length > 1) ? EOL : ""));
    await waitDrain();

    if (_commands.length > 0) _sendRequest();
     else _enterIdle();

    //Read what's in socket after every writing
   receiveData();
 }

 void receiveData() {
   _socket.read(false).then((res) {
     _incommingBuffers.add(res);
     _parseIncommingCommands(_iterateIncommingBuffers());
     process.....;
   });
 }

 void _parseIncommingCommands(Iterable<List<int>> arrIt) {
    for (List<int> command in arrIt) {
      _clearIdle();
      if (command[0] == ASCII_PLUS) {
        if (_currentCmd.data.length > 0) {
          String chunk = _currentCmd.data[0];
          _currentCmd.data = _currentCmd.data.substring(1);
          chunk += (_currentCmd.data.length > 0 ? EOL : '');
          send(chunk);
        } else if (_currentCmd.errorResponseExpectsEmptyLine)  send(EOL);
        continue;
      }

      parser.IResponse response;
      try {
        response = parser.parse(command);

      } catch (err) {
        print(err.toString());
        if (onError != null) onError(err);
      }

      _processResponse(response);
      _handleResponse(response);

      if (!_connectionReady) {
        _connectionReady = true;
        if (onReady != null) onReady();
      }
    }
 }

 void _handleResponse(parser.IResponse response) {
    String command = (response.command ?? "").toUpperCase().trim();
    if (_currentCmd != null) {
      if (response.tag == "*" && _globalAcceptUntagged[command] != null)
        _globalAcceptUntagged[command](response);
       else
         if (response.tag == '*' && _currentCmd.payload[command] != null) {
          //Executed untagged response
           _currentCmd.payload[command].add(response);
         } else if (response.tag == '*' && _globalAcceptUntagged[command] != null) {
          //Unexpected untagged response
           _globalAcceptUntagged[command](response);
         } else if (response.tag == _currentCmd.tag) {
          //Tagged response
           if (_currentCmd.payload.length > 0)
              //response.payLoad = _currentCmd.payload; ???????

           _currentCmd.callback(response);
           //_canSend = true;
           _sendRequest();
         }
    }
 }

  Iterable<List<int>> _iterateIncommingBuffers() sync* {
   List<int> buf = _incommingBuffers[_incommingBuffers.length -
       1]; //Get last element
   int i = 0;

   while (i < buf.length) {
     switch (_bufState) {
       case BufferState.LITERAL:
         int diff = min(buf.length - i, _literalRemaining);
         _literalRemaining -= diff;
         i += diff;
         if (_literalRemaining == 0) _bufState = BufferState.DEFAULT;
         continue;
       case BufferState.LITERAL_LEN_2:
         if (i < buf.length) {
           if (buf[i] == CARRIAGE_RETURN) {
             _literalRemaining =
                 int.parse(mime.MimeCodec.fromTypedArray(_lengthBuffer)) +
                     2; //for CRLF+2bytes
             _bufState = BufferState.LITERAL;
           } else
             _bufState = BufferState.DEFAULT;

           _lengthBuffer = null; //release buffer for garbage collector
         }
         continue;
       case BufferState.LITERAL_LEN_1:
         int start = i;
         while (i < buf.length && buf[i] > 48 && buf[i] < 57) i++; //digits !
         if (start != i) {
           List<int> latest = buf.getRange(start, i).toList();
           List<int> prevBuf = _lengthBuffer;
           _lengthBuffer = new List(); //prevBuf.length+latest.length
           _lengthBuffer.addAll(prevBuf);
           _lengthBuffer.addAll(latest);
         }

         if (i < buf.length) {
           if (_lengthBuffer.length > 0 && buf[i] == RIGHT_CURLY_BRACKET)
             _bufState = BufferState.LITERAL_LEN_2;
           else {
             _lengthBuffer = null; //release length buffer
             _bufState = BufferState.DEFAULT;
           }
           i++;
         }
         continue;

       default:
         int leftIdx = buf.indexOf(LEFT_CURLY_BRACKET, i);
         if (leftIdx > -1) {
           List<int> leftOfLeftCurly = buf.getRange(i, leftIdx - i).toList();
           if (leftOfLeftCurly.indexOf(LINE_FEED) == -1) {
             i = leftIdx + 1;
             _lengthBuffer = new List();
             _bufState = BufferState.LITERAL_LEN_1;
             continue;
           }
         }

         int lfidx = buf.indexOf(LINE_FEED, i);
         if (lfidx > -1) {
           if (lfidx < buf.length - 1)
             _incommingBuffers[_incommingBuffers.length - 1] =
                 buf.getRange(0, lfidx + 1).toList();

           int commandLength = -2; //Start from -2 (CRLF remove)
           for (int j = 0; j < _incommingBuffers.length; j++)
             commandLength += _incommingBuffers[j].length;
           List<int> command = new List(commandLength);
           int index = 0;

           while (_incommingBuffers.length > 0) {
             List<int> arr = _incommingBuffers[0]; //get first array
             _incommingBuffers.removeAt(0); //and remove it from list
             int remainingLength = commandLength - index;

             int arrlen = arr.length;
             if (arrlen > remainingLength) {
               int excessLength = arrlen - remainingLength;
               arr = arr.getRange(0, arrlen - excessLength).toList();

               if (_incommingBuffers.length > 0) _incommingBuffers = new List();
             }
           }
           yield command;

           if (lfidx < buf.length - 1) {
             buf = buf.getRange(lfidx + 1, buf.length).toList();
             _incommingBuffers.add(buf);
             i = 0;
           } else {
             _socketTimeout.cancel();
             _socketTimeout = null;
             return;
           }
         } else {
           return;
         }
     }
   }
 }

  Future<parser.IResponse> exec(compiler.IRequest request, [List<String> acceptUntagged, Map<String,String> options]) {
   //_clearIdle();
   acceptUntagged = acceptUntagged ?? [];
   options = options ?? {};

   Completer<parser.IResponse> comp = new Completer();
   addRequest(request, acceptUntagged, options, (parser.IResponse resp) {
     if (resp.capability != null) _capability = resp.capability;
     comp.complete(resp);
    });

   return comp.future;
  }

  dynamic _processResponse(parser.IResponse resp) {
    String command = (resp.command ?? "").toUpperCase().trim();
    if ((resp.attributes == null) || (resp.attributes.length == 0)) return;
    if (resp.tag == '*' && (resp.command.indexOf(new RegExp(r'^\d+$')) > -1) &&
        resp.attributes[0] is parser.SNode &&
        resp.attributes[0].type == 'ATOM') {
          try { resp.nr = int.parse(resp.command); } catch(err) { print(err.toString()+" parsing command number"); }

          resp.command = (resp.attributes[0] as parser.SNode).value.toUpperCase().trim();
          resp.attributes.removeAt(0);
    }

    //No optional response code
    if (['OK', 'NO', 'BAD', 'BYE', 'PREAUTH'].indexOf(command) == -1) return;

    //If last element of the response is TEXT then this is for humans
    if ((resp.attributes[resp.attributes.length-1] as parser.SNode).type == 'TEXT')
      resp.humanReadable = (resp.attributes[resp.attributes.length-1] as parser.SNode).value;

    //Parse and format ATOM values
    parser.SNode attr = resp.attributes[0] as parser.SNode;
    if (attr.type == 'ATOM' && attr.section != null && attr.section.length > 0) {
      List<String> option = attr.section.map((parser.SNode key) {
        if (key == null) return null;
        return key.value.toUpperCase().trim();
      }).toList();

      resp.code = option[0];
      option.removeAt(0);
      resp.option = option;
    }
  }

  Future updateId(dynamic id) {
    if (_capability.indexOf('ID') == -1) return new Future<dynamic>.value(null);
    List<String> attributes = new List();
    if (id != null) {
      if (id is String) id = <String, String> {'name': id as String};
      if (id is Map)
        id.forEach((dynamic key, dynamic val) =>
        attributes
          ..add(key as String)
          ..add(val as String));
    } else attributes.add(null);

    return exec(new compiler.IRequest()
      ..command = 'ID'
      ..attributes = attributes, ['ID']).then<dynamic>((parser.IResponse resp) {
        if (resp.payLoad.length == null || resp.payLoad["ID"] == null || resp.payLoad["ID"].length == 0)
          return null;

        serverId = {};
        String key;
        /*
        String attr = resp.payLoad['ID'][0]
    var key;
    [].concat([].concat(response.payload.ID.shift().attributes || []).shift() || []).forEach((val, i) => {
        if (i % 2 === 0) {
      key = (val && val.value || '').toString().toLowerCase().trim();
    } else {
    this.serverId[key] = (val && val.value || '').toString();
        */
    });
  }


}

}

/*
// Event placeholders
this.oncert = null;
this.onupdate = null;
this.onselectmailbox = null;
this.onclosemailbox = null;
*/

/*
// Default handlers for untagged responses
this.client.setHandler('capability', (response) => this._untaggedCapabilityHandler(response)); // capability updates
this.client.setHandler('ok', (response) => this._untaggedOkHandler(response)); // notifications
this.client.setHandler('exists', (response) => this._untaggedExistsHandler(response)); // message count has changed
this.client.setHandler('expunge', (response) => this._untaggedExpungeHandler(response)); // message has been deleted
this.client.setHandler('fetch', (response) => this._untaggedFetchHandler(response)); // message has been updated (eg. flag change)
*/
