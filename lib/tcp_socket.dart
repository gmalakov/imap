library tcp_socket;

import 'dart:io';
import 'dart:async';

const EVENT_INBOUND = 'inbound';
const EVENT_OUTBOUND = 'outbound';
const EVENT_OPEN = 'open';
const EVENT_CLOSE = 'close';
const EVENT_ERROR = 'error';
const EVENT_CONFIG = 'configure';
const EVENT_CERT = 'cert';
const EVENT_HANDSHAKE = 'handshake';

enum Ready { connecting }

class MailSocket {
  bool _useSecure = false;
  String _host;
  int _port;
  bool get ssl => _useSecure;
  int bufferedAmount = 0;

  Socket _sock;
  SecureSocket _ssock;

  MailSocket(this._host, this._port, this._useSecure);

  Future<bool> connect() async {
    if (ssl) _ssock = await SecureSocket.connect(_host, _port, onBadCertificate: (X509Certificate c) {
      return true;
    }); else
      _sock = await Socket.connect(_host, _port);

    if (ssl) return (_ssock != null);
     else return (_sock != null);
  }

  void write(List<int> bytes) {
    if (ssl) _ssock.write(bytes);
     else _sock.write(bytes);
  }

  Future<List<int>> read(bool cloneOnDone) {
    Completer<List<int>> comp = new Completer();
    if (ssl) //Is ssl and is connected ??
      if (_ssock == null) return null;
        else if (_sock == null) return null;

    if (ssl) _ssock.listen((List<int> data) => comp.complete(data), onDone: () {
      if (cloneOnDone) _ssock.destroy();
    });
     else _sock.listen((List<int> data) => comp.complete(data), onDone: () {
      if (cloneOnDone) _sock.destroy();
    });
    return comp.future;
  }

  void close() {
    if (ssl) _ssock?.destroy();
     else _sock?.destroy();
  }

}


