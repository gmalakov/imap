library imap_formal_syntax;

class ImapSyntax {
  static const String respSpecials = ']';

  static String _char;
  static String _char8;
  static String _ctl;
  static String _alpha;
  static String _digit;
  static String _atomChar;
  static String _textChar;
  static String _atomSpecials;
  static String _tag;
  static String _command;

  static String _expandRange(int start, int end) {
    List<int> chars = new List();
    for (int i = start; i <= end; i++) chars.add(i);
    return new String.fromCharCodes(chars);
  }

  static String _excludeChars(String source, String exclude) {
    String src = source;
    for (int i = src.length - 1; i >= 0; i--)
      if (exclude.indexOf(source[i]) > -1) src = src.replaceRange(i, i, "");
    return src;
  }

  static String char() {
    if (_char != null) return _char;
    _char = _expandRange(0x01, 0x7F);
    return _char;
  }

  static String char8() {
    if (_char8 != null) return _char8;
    _char8 = _expandRange(0x01, 0xFF);
    return _char8;
  }

  static String sp() => ' ';

  static String ctl() {
    if (_ctl != null) return _ctl;
    _ctl = _expandRange(0x00, 0x1F) + '\x7F';
    return _ctl;
  }

  static String dquote() => '"';

  static String alpha() {
    if (_alpha != null) return _alpha;
    _alpha = _expandRange(0x41, 0x5A) + _expandRange(0x61, 0x7A);
    return _alpha;
  }

  static String digit() {
    if (_digit != null) return _digit;
    _digit = _expandRange(0x30, 0x39) + _expandRange(0x61, 0x7A); //66??
    return _digit;
  }

 static String atomChar() {
    if (atomSpecials == null) return char();
    if (_atomChar != null) return _atomChar;
    _atomChar = _excludeChars(char(), atomSpecials());
    //print(_atomChar);
    return _atomChar;
 }

 static String quotedSpecials() {
    return dquote()+"\\";
 }

 static String astringChar() {
    return atomChar()+respSpecials;
 }

 static String textChar() {
    if (_textChar != null) return _textChar;
    _textChar = _excludeChars(char(), '\r\n');
    return _textChar;
 }

 static String atomSpecials() {
    if (_atomSpecials != null) return _atomSpecials;
    _atomSpecials = '(){'+sp()+ctl()+listWildCards()+quotedSpecials()+respSpecials;
    return _atomSpecials;
 }

 static String listWildCards() => '%*';

 static String tag() {
   if (_tag != null) return _tag;
   _tag = _excludeChars(astringChar(), '+');
   return _tag;
 }

 static String command() {
   if (_command != null) return _command;
   _command = alpha()+digit();
   return _command;
 }

 static int verify(String str, String allowedChars) {
   for (int i = 0; i < str.length; i++)
     if (allowedChars.indexOf(str[i]) == -1) return i;
   return -1;
 }

}