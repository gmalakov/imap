library imap_parser;

import 'imap_formal_syntax.dart';

const ASCII_NL = 10;
const ASCII_CR = 13;
const ASCII_SPACE = 32;
const ASCII_LEFT_BRACKET = 91;
const ASCII_RIGHT_BRACKET = 93;

/*
enum ICommand {SEARCH, FETCH, ID, LIST, LSUB, NAMESPACE }

class PayLoad {
  ICommand command;
  List<String> data;
} */

class IResponse {
  Map<String, List<String>> payLoad;
  List<String> capability;
  String code;
  String command;
  String tag;
  String humanReadable;
  List<dynamic> attributes;
  List<String> option;
  int nr;
}

class ImapParser {
  static String fromCharCode(List<int> arr, [List<int> skip]) {
    skip = skip ?? new List(); //init empty list if null is set
    const max = 10240;
    List<int> oList = new List();

    if (max < arr.length - skip.length) arr.removeRange(max+skip.length, arr.length);
    for (int i = 0; i < arr.length; i++)
      if (skip.indexOf(i) == -1) oList.add(arr[i]);

    return new String.fromCharCodes(oList);
  }

  static String fromCharCodeTrimmed(List<int> arr, [List<int> skip]) {
    int begin = 0;
    int end = arr.length;
    while(arr[begin] == ASCII_SPACE) begin++;
    while(arr[end] == ASCII_SPACE) end--;

    return fromCharCode(arr.getRange(begin, end).toList(), skip);
  }

  static bool isEmpty(List<int> arr) {
    for (int i = 0; i < arr.length; i++)
      if (arr[i] != ASCII_SPACE) return false;

    return true;
  }

}

class ParserInstance {
  List<int> remainder;
  Map<String, dynamic> options;
  int pos = 0;
  String humanReadable;
  String _command;

  String _tag;

  ParserInstance(this.remainder, this.options);

  String getTag() {
    if (_tag != null) return _tag;
    _tag = getElement(ImapSyntax.tag()+'*+'); //true??
    return _tag;
  }

  String getCommand() {
    if (_command == null)
    _command = getElement(ImapSyntax.command());

    switch ((_command ?? "").toUpperCase()) {
      case 'OK':
      case 'NO':
      case 'BAD':
      case 'PREAUTH':
      case 'BYE':
        int lastRightBracket = remainder.lastIndexOf(ASCII_RIGHT_BRACKET);
        if (remainder[1] == ASCII_LEFT_BRACKET && lastRightBracket > 1) {
          humanReadable =
              ImapParser.fromCharCodeTrimmed(remainder.getRange(lastRightBracket+1, remainder.length).toList());
          remainder = remainder.getRange(0, lastRightBracket+1).toList();
        } else {
          humanReadable = ImapParser.fromCharCodeTrimmed(remainder);
          remainder = new List();
        }

        break;
    }

    return _command;
  }

  String getElement(String syntax) {
    String element;
    int errPos;

    if (remainder[0] == ASCII_SPACE) throw new Exception('Unexpected whitespace at position $pos');
    int firstSpace = remainder.indexOf(ASCII_SPACE);
    if (remainder.length > 0 && firstSpace != 0) {
      if (firstSpace == -1)
        element = ImapParser.fromCharCode(remainder);
      else
        element =
            ImapParser.fromCharCode(remainder.getRange(0, firstSpace).toList());

      errPos = ImapSyntax.verify(element, syntax);
      if (errPos > -1) throw new Exception(
          'Unexpected char at position ' + (pos + errPos).toString());
    } else
      throw new Exception('Unexpected end of input at position ' + pos.toString());

    pos += element.length;
    remainder = remainder.getRange(element.length, remainder.length).toList();

    return element;
  }

  void getSpace() {
    if (remainder.length == 0) throw new Exception('Unexpected end of input at position $pos');
    String r = new String.fromCharCode(remainder[0]);
    if (ImapSyntax.verify(r, ImapSyntax.sp()) > -1)
      throw new Exception('Unexpected char at position $pos $r');

    pos++;
    remainder.removeAt(0); //Remove first char
  }

  List getAttributes() {
   if (remainder.length == 0) throw new Exception('Unexpected end of input at position $pos');
   if (remainder[0] == ASCII_SPACE) throw new Exception('Unexpected whitespace at position $pos');

   //copy remainder to new value here!
   return new TokenParser(this, pos, remainder.getRange(0, remainder.length).toList(), options).getAttributes();
  }
}

class Node {
  Node parentNode;

  List<int> arr;
  List<Node> childNodes = new List();
  String type;
  bool closed = true;
  List<int> valueSkip = new List();
  int startPos;
  int valueStart;
  int valueEnd;
  int endPos;
  bool valueToUpperCase = false;

  int literalLength;
  bool started;
  bool literalPlus;

  Node(this.arr, this.parentNode, this.startPos) {
    if (parentNode != null) parentNode.childNodes.add(this); //Set myself in childnodes of parent node
    valueStart = (startPos ?? -1)+1;
 }

  Map<String, dynamic> toMap() => <String, dynamic>{
    "type": type,
    //"childNodes": childNodes.map((Node el) => el.toMap()).toList(),
    "valuePos": "$valueStart -> $valueEnd",
    "pos": "$startPos -> $endPos"
    //"value": getValue()
  };

  String toString() => toMap().toString();


  String getValue() {
    String value = ImapParser.fromCharCode(arr.getRange(valueStart, valueEnd).toList(),valueSkip);
    if (valueToUpperCase) return value.toUpperCase();
     else return value;
  }

  int getValueLength() {
    return valueEnd - valueStart - valueSkip.length;
  }

  bool equals(String value, [bool caseSensitive = true]) {
    if (getValueLength() != value.length) return false;
    return equalsAt(value, 0, caseSensitive);
  }

  bool equalsAt(String value, int index, [bool caseSensitive = true]) {
    index = index ?? 0;
    if (index < 0) {
      index = valueEnd + index;
      while (valueSkip.indexOf(valueStart+index) > -1) index--;
    } else
      index = valueStart+index;

    for (int i = 0; i < value.length; i++) {
      while (valueSkip.indexOf(index - valueStart) > -1) index++;
      if (index >= valueEnd) return false;

      if (index < 0) return null;
      String lchr = new String.fromCharCode(arr[index]);
      String  chr = value[i];

      if (!caseSensitive) {
        lchr = lchr.toUpperCase();
        chr = chr.toUpperCase();
      }

      if (lchr != chr)  return false;
      index++;
    }

    return true;
  }

  bool isNumber() {
    for (int i = 0; i < valueEnd - valueStart; i++) {
      if (valueSkip.indexOf(i) > -1) continue;
      if (!this.isDigit(i)) return false;
    }

    return true;
  }

  bool isDigit(int idx) {
    if (idx < 0) {
      idx = valueEnd+idx;
      while (valueSkip.indexOf(valueStart+idx) > -1) idx--;
    } else {
      idx = valueStart+idx;
      while (valueSkip.indexOf(valueStart+idx) > -1) idx++;
    }

    int ascii = arr[idx];
    return (ascii > 47) && (ascii < 58);
  }

  bool containsChar(String chr) {
    int ascii = chr.codeUnitAt(0);
    for (int i = valueStart; i < valueEnd; i++) {
      if (valueSkip.indexOf(i-valueStart) > -1) continue;
      if (arr[i] == ascii) return true;
    }

    return false;
  }
}

class SNode {
  String type;
  String value;
  List<SNode> section = new List();
  List<int> partial = new List();


  SNode(this.type, this.value);

  Map<String, dynamic> toMap() => <String, dynamic>{
    "type": type,
    "value": value,
    "section": section.map((el) => el.toMap()),
    "partial": partial
  };

  String toString() => toMap().toString();
}

class TokenParser {
  List<int> arr;
  Map<String, dynamic> options;
  ParserInstance parent;
  Node currentNode;
  Node tree;
  int pos;
  String state = 'NORMAL';

  int parseInt(dynamic val) {
    if (val is int) return val;
    int rV;
    if (val is String) {
      if (val.indexOf(new RegExp(r'[A-Fa-f]')) > -1) //\d
        try {
          rV = int.parse(val, radix: 16);
        } catch (err) {}
      else
        try {
          rV = int.parse(val);
        } catch (err) {}
    }

    return rV;
  }

  TokenParser(this.parent, int startPos, this.arr, this.options) {
    currentNode = createNode(null, 0);
    currentNode.type = 'TREE';
    tree = currentNode;
    pos = startPos ?? 0;
    processString();
  }

  List<dynamic> getAttributes() {
    List<dynamic> attributes = new List<dynamic>();
    List<dynamic> branch = attributes;

    void walk(Node node) {
      List<dynamic> curBranch = branch;

      if (!node.closed && node.type == 'SEQUENCE' && node.equals('*'))
        node
          ..closed = true
          ..type = 'ATOM';

      if (!node.closed) throw new Exception(
          'Unexpected end of input at position ' +
              (pos + arr.length - 1).toString() +
              "\n" + node.toString());

      switch (node.type.toUpperCase()) {
        case 'LITERAL':
        case 'STRING':
        case 'SEQUENCE':
          SNode elm = new SNode(node.type.toUpperCase(), node.getValue());
          branch.add(elm);
          break;
        case 'ATOM':
          if (node.equals('NIL', true)) {
            branch.add(null);
            break;
          }
          SNode elm = new SNode(node.type.toUpperCase(), node.getValue());
          branch.add(elm);
          break;
        case 'SECTION':
          branch = branch[branch.length - 1].section = new List<dynamic>();
          break;
        case 'LIST':
          List<SNode> elm = new List();
          branch.add(elm);
          branch = elm;
          break;
        case 'PARTIAL':
          List<int> partial = node.getValue().split('.')
              .map((el) => parseInt(el)).toList();

          branch[branch.length - 1].partial = partial;
          break;
      }

      node.childNodes.forEach((el) => walk(el));
      branch = curBranch;
    }

    walk(tree);
    return attributes;
  }

  Node createNode(Node parentNode, int startPos) =>
      new Node(arr, parentNode, startPos);

  void processString() {
    int i;
    int len;

    void checkSP() {
      while (i < (arr.length-1) && (arr[i + 1] == ASCII_SPACE) ) i++;
    }

    len = arr.length;
    for (i = 0; i < len; i++) {
      String chr = new String.fromCharCode(arr[i]);
      switch (state) {
        case 'NORMAL':
          switch (chr) {
            case '"':
              currentNode = createNode(currentNode, i);
              currentNode.type = 'string';
              state = 'STRING';
              currentNode.closed = false;
              break;

            case '(':
              currentNode = createNode(currentNode, i);
              currentNode.type = 'LIST';
              currentNode.closed = false;
              break;

            case ')':
              if (currentNode.type != 'LIST')
                throw new Exception(
                    'Unexpected list terminator ) at position ${pos + i}');
              currentNode
                ..closed = true
                ..endPos = pos + i;

              //Level up
              currentNode = currentNode.parentNode;
              checkSP();
              break;

            case ']':
              if (currentNode.type != 'SECTION')
                throw new Exception(
                    'Unexpected section terminator ] at position ${pos + i}');
              currentNode
                ..closed = true
                ..endPos = pos + i;

              currentNode = currentNode.parentNode;
              checkSP();
              break;

            case '<':
              if (new String.fromCharCode(arr[i - 1]) != ']') {
                currentNode = createNode(currentNode, i);
                currentNode
                  ..type = 'ATOM'
                  ..valueStart = i
                  ..valueEnd = i + 1;

                state = 'ATOM';
              } else {
                currentNode = createNode(currentNode, i);
                currentNode
                  ..type = 'PARTIAL';
                state = 'PARTIAL';
                currentNode.closed = false;
              }
              break;

            case '{':
              currentNode = createNode(currentNode, i);
              currentNode
                ..type = 'LITERAL'
                ..closed = false;
              state = 'LITERAL';
              break;

            case '*':
              currentNode = createNode(currentNode, i);
              currentNode
                ..type = 'SEQUENCE'
                ..valueStart = i
                ..valueEnd = i + 1
                ..closed = false;
              state = 'SEQUENCE';
              break;

            case ' ':
              break;

            case '[':
              String cmd = parent.getCommand();
              if (['OK', 'NO', 'BAD', 'BYE', 'PREAUTH'].indexOf(
                  cmd.toUpperCase()) > -1 &&
                  currentNode == tree) {
                currentNode.endPos = pos + i;

                currentNode = createNode(currentNode, i);
                currentNode.type = 'ATOM';
                currentNode = createNode(currentNode, i);
                currentNode
                  ..type = 'SECTION'
                  ..closed = false;

                state = 'NORMAL';

                if (ImapParser.fromCharCode(
                    arr.getRange(i + 1, i + 10).toList()).toUpperCase() ==
                    "REFERRAL ") {
                  currentNode = createNode(currentNode, pos + i + 1);
                  currentNode
                    ..type = 'ATOM'
                    ..endPos = pos + i + 8
                    ..valueStart = i + 1
                    ..valueEnd = i + 9
                    ..valueToUpperCase = true;

                  currentNode = currentNode.parentNode;

                  currentNode = createNode(currentNode, pos + i + 10);
                  currentNode
                    ..type = 'ATOM';
                  i = arr.indexOf(ASCII_RIGHT_BRACKET, i + 10);
                  currentNode
                    ..endPos = pos + i - 1
                    ..valueStart = currentNode.startPos - pos
                    ..valueEnd = currentNode.endPos - pos + 1;

                  currentNode = currentNode.parentNode;

                  currentNode.closed = true;
                  currentNode = currentNode.parentNode;
                  checkSP();
                }
              }
             break;

            default:
              if (ImapSyntax.atomChar().indexOf(chr) == -1 && (chr != '\\') &&
                  (chr != '%'))
                throw new Exception('Unexpected char at position ${pos + i}');

              currentNode = createNode(currentNode, i);
              currentNode
                ..type = 'ATOM'
                ..valueStart = i
                ..valueEnd = i + 1;
              state = 'ATOM';
              break;
          }
          break;

        case 'ATOM':
          if (chr == ' ') {
            currentNode.endPos = pos + i - 1;
            currentNode = currentNode.parentNode;
            state = 'NORMAL';
            break;
          }

          if (currentNode.parentNode != null &&
              (chr == ')' && currentNode.parentNode.type == 'LIST') ||
              (chr == ']' && currentNode.parentNode.type == 'SECTION')) {
            currentNode.endPos = pos + i - 1;
            currentNode = currentNode.parentNode;
            currentNode
              ..closed = true
              ..endPos = pos + 1;
            currentNode = currentNode.parentNode;
            state = 'NORMAL';

            checkSP();
            break;
          }

          if ((chr == ',' || chr == ':') && currentNode.isNumber()) {
            currentNode
              ..type = 'SEQUENCE'
              ..closed = true;
            state = 'SEQUENCE';
          }

          if (chr == '[' && (currentNode.equals('BODY', false) ||
              currentNode.equals('BODY.PEEK', false))) {
            currentNode.endPos = pos + i;
            currentNode = createNode(currentNode, pos + i);
            currentNode
              ..type = 'SECTION'
              ..closed = false;
            state = 'NORMAL';
            break;
          }

          if (chr == '<') throw new Exception(
              'Unexpected start of partial at position $pos .');
          if (ImapSyntax.atomChar().indexOf(chr) == -1 && chr != ']'
              && !(chr == '*' && currentNode.equals('\\')))
            throw new Exception('Unexpected char at position ${pos + i}');
          else if (currentNode.equals('\\*')) throw new Exception(
              'Unexpected char at position ${pos + i}');

          currentNode.valueEnd = i + 1;
          break;

        case 'STRING':
          if (chr == '"') {
            currentNode
              ..endPos = pos + i
              ..closed = true;
            currentNode = currentNode.parentNode;
            state = 'NORMAL';

            checkSP();
            break;
          }

          if (chr == '\\') {
            currentNode.valueSkip.add(i - currentNode.valueStart);
            i++;
            if (i >= len) throw new Exception(
                'Unexpected end of input at position ${pos + i}');

            chr = new String.fromCharCode(arr[i]);
          }

          currentNode.valueEnd = i + 1;
          break;

        case 'PARTIAL':
          if (chr == '>') {
            if (currentNode.equalsAt('.', -1))
              throw new Exception('Unexpected end of partial at position $pos');

            currentNode
              ..endPos = pos + i
              ..closed = true;
            currentNode = currentNode.parentNode;
            state = 'NORMAL';
            checkSP();
            break;
          }
          if (chr == '.' && (currentNode.getValueLength() == 0 ||
              currentNode.containsChar(".")))
            throw new Exception(
                'Unexpected partial separator . at position $pos');

          if (ImapSyntax.digit().indexOf(chr) == -1 && chr != '.')
            throw new Exception('Unexpected char at position ${pos + i}');


          if (chr != '.' &&
              (currentNode.equals('0') || currentNode.equalsAt('.0', -2)))
            throw new Exception('Invalid partial at position ${pos + i}');

          this.currentNode.valueEnd = i + 1;
          break;

        case 'LITERAL':
          if (currentNode.started) {
            if (chr == '\u0000') throw new Exception(
                'Unexpected \\x00 at position ${pos + i}');
            currentNode.valueEnd = i + 1;

            if (currentNode.getValueLength() >= currentNode.literalLength) {
              currentNode.endPos = pos + i;
              currentNode.closed = true;
              currentNode = currentNode.parentNode;
              state = 'NORMAL';
              checkSP();
            }
            break;
          }

          if (chr == '+' && this.options["literalPlus"] as bool) {
            this.currentNode.literalPlus = true;
            break;
          }

          if (chr == '}') {
            if (currentNode.literalLength == null)
              throw new Exception(
                  'Unexpected literal prefix end char } at position ${pos +
                      i}');
            if (arr[i + 1] == ASCII_NL)
              i++;
            else if (arr[i + 1] == ASCII_CR && arr[i + 2] == ASCII_NL)
              i += 2;
            else
              throw new Exception('Unexpected char at position ${pos + i}');

            currentNode
              ..valueStart = i + 1
              ..started = true;

            if (currentNode.literalLength == null) {
              currentNode
                ..endPos = pos + i
                ..closed = true;
              currentNode = currentNode.parentNode;
              state = 'NORMAL';
              checkSP();
            }
            break;
          }

          if (ImapSyntax.digit().indexOf(chr) == -1)
            throw new Exception('Unexpected char at position ${pos + i}');
          if (currentNode.literalLength == 0)
            throw new Exception('Invalid literal at position ${pos + i}');

          currentNode.literalLength =
              (currentNode.literalLength ?? 0) * 10 + parseInt(chr);
          break;

        case 'SEQUENCE':
          if (chr == ' ') {
            if (currentNode.isDigit(-1) && !currentNode.equalsAt('*', -1))
              throw new Exception(
                  'Unexpected whitespace at position ${pos + i}');

            //if (currentNode.equalsAt('*', -1) && !currentNode.equalsAt(':', -2))
            //  throw new Exception('Unexpected whitespace at position ${pos + i}');

            currentNode
              ..closed = true
              ..endPos = pos + i - 1;
            currentNode = currentNode.parentNode;
            state = 'NORMAL';
            break;
          } else if (currentNode.parentNode != null &&
              chr == ']' && currentNode.parentNode.type == 'SECTION') {
            currentNode.endPos = pos + i - 1;
            currentNode = currentNode.parentNode;
            currentNode.closed = true;
            currentNode.endPos = pos + i;
            currentNode = currentNode.parentNode;
            state = 'NORMAL';

            checkSP();
            break;
          }

          if (chr == ':') {
            if (!currentNode.isDigit(-1) && currentNode.equalsAt('*', -1))
              throw new Exception(
                  'Unexpected range separator : at position ${pos + i}');
          } else if (chr == '*') {
            if (currentNode.equalsAt(',', -1) &&
                !currentNode.equalsAt(':', -1))
              throw new Exception(
                  'Unexpected range wildcard at position ${pos + i}');
          } else if (chr == ',') {
            if (!currentNode.isDigit(-1) && !currentNode.equalsAt('*', -1))
              throw new Exception(
                  'Unexpected sequence separator , at position ${pos + i}');
            if (currentNode.equalsAt('*', -1) &&
                !currentNode.equalsAt(':', -2))
              throw new Exception(
                  'Unexpected sequence separator , at position ${pos + i}');
          } else if (chr.indexOf(new RegExp(r'\d')) == -1)
            throw new Exception('Unexpected char at position ${pos + i}');

          if (chr.indexOf(new RegExp(r'\d')) > -1 &&
              currentNode.equalsAt('*', -1))
            throw new Exception('Unexpected number at position ${pos + i}');

          this.currentNode.valueEnd = i + 1;
          break;
      }
    }
  }

}

IResponse parse(List<int> buffer, [Map<String, dynamic> options]) {
  options = options ?? <String, dynamic>{};
  IResponse ret = new IResponse();
  var myParser = new ParserInstance(buffer, options);
  ret.tag = myParser.getTag();
  myParser.getSpace();
  ret.command = myParser.getCommand();

  if (['UID', 'AUTHENTICATE'].indexOf((ret.command ?? '').toUpperCase()) > -1) {
    myParser.getSpace();
    ret.command += ' ' + myParser.getElement(ImapSyntax.command());
  }

  if (myParser.remainder.length > 0) {
    myParser.getSpace();
    ret.attributes = myParser.getAttributes() as List<List<String>>; //???
  }

  if (myParser.humanReadable != null)
    ret.attributes = (ret.attributes ?? new List<dynamic>())..add(myParser.humanReadable);

  return ret;
}

