import '../lib/string_encoding.dart';

void main() {
  var decode = new TextDecoder('utf-8');
  var encode = new TextEncoder('gb18030');

  BytesInputStream bis = new BytesInputStream.fromString("Ей как ли ще е на китайски");
  BytesOutputStream bos = new BytesOutputStream();
  for (int i = 0; i < bis.bytes.length; i ++) {
    decode.decode(bis);
    encode.encode(bos, bis);
  }

}
