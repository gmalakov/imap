library base64;

class Base64 {
  static const String _ALPHA = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
//  static const String PADCHAR = "=";

  static String encode(List<int> inArr,{String padChar = "="}) {
    int imax = inArr.length - inArr.length % 3;
    int i = 0;
    String x = "";

    if (inArr.isEmpty) return ""; //Return empty string on empty input

    for (i = 0; i < imax; i += 3) {
      int b10 = (inArr[i] << 16) | ( inArr[i+1] << 8) | inArr[i+2];
      x += (_ALPHA[b10 >> 18]);
      x += (_ALPHA[(b10 >> 12) &  0x3F ]);
      x += (_ALPHA[(b10 >> 6) & 0x3F]);
      x += (_ALPHA[b10 & 0x3F]);
    }

    switch (inArr.length - imax) {
      case 1:
        int b10 = inArr[i] << 16;
        x += (_ALPHA[b10 >> 18]) + (_ALPHA[(b10 >> 12) & 0x3F]) + padChar + padChar;
        break;

      case 2:
        int b10 = (inArr[i] << 16) | (inArr[i+1] << 8);
        x += (_ALPHA[b10 >> 18]) + (_ALPHA[(b10 >> 12) & 0x3F]) + (_ALPHA[(b10 >> 6) & 0x3F]) + padChar;
        break;
    }

    return x;
  }

  static List<int> decode (String base64Str) {
    int bitsSoFar = 0;
    int validBits = 0;
    int iOut = 0;
    List<int> baseArr = base64Str.codeUnits;
    int len = baseArr.length;
    List<int> arr = new List<int>((len * 3 / 4).ceil());
    int bits;

    for (int i = 0; i < len; i++) {
      int c = baseArr[i];

      if ((c >= 0x41) && (c <= 0x5a)) // A-Z
        bits = c - 0x41;
      else if ((c >= 0x61) && (c <= 0x7a)) // a-z
        bits = c - 0x47; // - 0x61 + 0x1a;
      else if ((c >= 0x30) && (c <= 0x39)) //  0-9
        bits = c + 0x4; // - 0x30 + 0x34
      else if (c == 0x2b) // +
        bits = 0x3e;
      else if (c == 0x2f) // /
        bits = 0x3f;
      else if (c == 0x3d) { // =
        validBits = 0; continue;
      }
      else {
        //bitsSoFar = 0; validBits = 0;
        continue;
      }
      //All other chars are ignored

      bitsSoFar = (bitsSoFar << 6) | bits;
      validBits += 6;
      if (validBits >= 8) {
        validBits -= 8; //Remove 8 bits
        arr[iOut++] = (bitsSoFar >> validBits) & 0xFF; //Only last 8 bits
        if (validBits == 2)
          bitsSoFar &= 0x03;
        else if (validBits == 4) bitsSoFar &= 0x0f;
      }
    }

    if (iOut < arr.length)
      return arr.sublist(0, iOut);
    else
      return arr;
  }
}
