part of address_parser;

class Tokenizer {
  static const Map<String, String> operators = const <String,String>{
    "\"": "\"",
    "(": ")",
    "<": ">",
    ",": "",
    // Groups are ended by semicolons
    ":": ";",
    // Semicolons are not a legal delimiter per the RFC2822 grammar other
    // than for terminating a group, but they are also not valid for any
    // other use in this context.  Given that some mail clients have
    // historically allowed the semicolon as a delimiter equivalent to the
    // comma in their UI, it makes sense to treat them the same as a comma
    // when used outside of a group.
    ";": ""
  };

  String operatorCurrent = "";
  String operatorExpecting = "";
  Node node = null;
  bool escaped = false;
  List<Node> list = new List();

  void checkChar(String char) {
    if ((operators.containsKey(char) || (char == "\\")) && escaped)
      escaped = false;
    else
    if (operatorExpecting.isNotEmpty && (char == operatorExpecting)) {
      node = new Node()
        ..type = "operator"
        ..value = char;

      list.add(node);
      node = null;
      operatorExpecting = "";
      escaped = false;

      return;
    } else if (operatorExpecting.isEmpty && operators.containsKey(char)) {
      node = new Node()
        ..type = "operator"
        ..value = char;

      list.add(node);
      node = null;
      operatorExpecting = operators[char];
      escaped = false;
      return;
    }

    if (!escaped && (char == "\\")) {
      escaped = true;
      return;
    }

    if (node == null) {
      node = new Node()
        ..type = "text"
        ..value = "";

      list.add(node);
    }

    if (escaped && (char != "\\"))
      node.value += "\\";

    node.value += char;
    escaped = false;

  }

  List<Node> tokenize(String str) {
    str = (str ?? "");
    String char;
    List<Node> oList = new List();

    for (int i = 0; i < str.length; i++) {
      char = str.substring(i,i+1);
      checkChar(char);
    }

    list.forEach((node) {
      if ((node.value != null) && (node.value.isNotEmpty)) {
        node.value = node.value.trim();
        oList.add(node);
      }
    });

    return oList;
  }

}