import '../lib/address_parser.dart';
import "package:test/test.dart";

const List<String> testAddr = const [
  "\"reinman; andris\" <andris@tr.ee>",
  "andris <andris@tr.ee>",
  "andris andris@tr.ee",
  "Disclosed:andris@tr.ee, andris@example.com;",
  "FirstName Surname-WithADash :: Company <firstname@company.com>",
  "andris@tr.ee (reinman) andris",
  "andris",
  "O'Neill",
  "Georgi Malakov <gmalakov@gmail.com>, Ludcho Ludchev  <ludchobg@gmail.com>"
];

const List<String> retAddr = const [
  "[{address: andris@tr.ee, name: reinman; andris, group: null}]",
  "[{address: andris@tr.ee, name: andris, group: null}]",
  "[{address: andris@tr.ee, name: andris, group: null}]",
  "[{address: , name: Disclosed, group: [{address: andris@tr.ee, name: , group: null}, {address: andris@example.com, name: , group: null}]}]",
  "[{address: , name: FirstName Surname-WithADash, group: [{address: , name: , group: [{address: firstname@company.com, name: Company, group: null}]}]}]",
  "[{address: andris@tr.ee, name: andris, group: null}]",
  "[{address: , name: andris, group: null}]",
  "[{address: , name: O'Neill, group: null}]",
  "[{address: gmalakov@gmail.com, name: Georgi Malakov, group: null}, {address: ludchobg@gmail.com, name: Ludcho Ludchev, group: null}]"
];

void main() {
  AddressParser addr = new AddressParser();

  for (int ca = 0; ca < testAddr.length; ca++) {
    test("Test address parsing exceptions ca=$ca", () {

      expect(addr.parseAddress(testAddr[ca]).toString(), equals(retAddr[ca]));
    });

  }
}